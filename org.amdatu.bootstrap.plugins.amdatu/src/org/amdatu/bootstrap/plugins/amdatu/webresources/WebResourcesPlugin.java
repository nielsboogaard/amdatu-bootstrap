package org.amdatu.bootstrap.plugins.amdatu.webresources;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import org.amdatu.bootstrap.core.BootstrapPlugin;
import org.amdatu.bootstrap.core.Command;
import org.amdatu.bootstrap.core.Dependency;
import org.amdatu.bootstrap.core.DependencyBuilder;
import org.amdatu.bootstrap.core.InstallResult;
import org.amdatu.bootstrap.core.Navigator;
import org.amdatu.bootstrap.core.PluginInfo;
import org.amdatu.bootstrap.core.ResourceManager;
import org.amdatu.bootstrap.core.InstallResult.Builder;
import org.amdatu.bootstrap.getopt.Arguments;
import org.amdatu.bootstrap.getopt.Description;
import org.amdatu.bootstrap.getopt.Parameters;
import org.amdatu.bootstrap.plugins.dependencymanager.DmService;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;

import aQute.bnd.build.model.BndEditModel;
import aQute.bnd.properties.Document;

@Component
@PluginInfo(name="web")
public class WebResourcesPlugin implements BootstrapPlugin{

	@ServiceDependency
	private volatile DependencyBuilder m_dependencyBuilder;
	
	@ServiceDependency
	private volatile Navigator m_navigator;
	
	@ServiceDependency
	private volatile ResourceManager m_resourceManager;
	
	@ServiceDependency
	private volatile DmService m_dmService;
	
	interface WebInstallArgs extends Parameters {
		@Description("Directory containing the static resources")
		String dir();
		
		@Description("Path to register")
		String path();
		
		@Description("Name of the default page")
		String defaultpage();
		
		@Description("Path to the bnd file")
		File bndfile();
	}

	@Command
	public void install(WebInstallArgs args) {
		String dir = args.dir();
		String path = args.path();
		String defaultpage = args.defaultpage();
		Path bndFile = m_navigator.getCurrentDir().resolve(args.bndfile().toPath());
		
		addWebResources(path, defaultpage, dir, bndFile);
	}
	
	private void addWebResources(String path, String defaultPath, String resourcesDir, Path bndFile) {
		if (!path.startsWith("/")) {
			path = "/" + path;
		}

		try {
			BndEditModel model = new BndEditModel();
			model.loadFrom(bndFile.toFile());

			model.addIncludeResource(resourcesDir + "=" + resourcesDir);
			model.genericSet("X-Web-Resource-Version", " 1.1");

			model.genericSet("X-Web-Resource", createWebResourceHeader(resourcesDir, path, model));

			if (defaultPath != null && !defaultPath.equals("")) {
				model.genericSet("X-Web-Resource-Default-Page", " " + defaultPath);
			}

			String bndContents = new String(Files.readAllBytes(bndFile));
			Document document = new Document(bndContents);
			model.saveChangesTo(document);

			m_resourceManager.writeFile(bndFile, document.get().getBytes());
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}
	}
	
	private String createWebResourceHeader(String resourcesDir, String path, BndEditModel model) {
		Object webResource = model.genericGet("X-Web-Resource");

		StringBuilder webResourceBuilder = new StringBuilder(" ");
		if (webResource != null) {
			webResourceBuilder.append(webResource).append(";");
		}

		webResourceBuilder.append(path);
		webResourceBuilder.append(";");
		webResourceBuilder.append(resourcesDir);
		return webResourceBuilder.toString();
	}

	
	
	@Arguments(arg = "runconfig...")
	interface RestRunArguments extends Parameters {
		List<File> arguments();
	}
	
	@Command
	public InstallResult run(RestRunArguments args) {
		List<File> runConfigs = args.arguments();
		Builder builder = InstallResult.builder();
		List<Dependency> deps = Dependency.fromStrings(
				"org.apache.felix.http.jetty", 
				"org.apache.felix.http.whiteboard", 
				"org.amdatu.web.resourcehandler", 
				"org.apache.felix.http.servlet-api",
				"org.apache.felix.http.api");
		
		runConfigs.stream()
			.map(File::toPath)
			.map(m_navigator.getCurrentDir()::resolve)
			.map(p -> m_dependencyBuilder.addRunDependency(deps, p))
			.forEach(builder::addResult);
	
		runConfigs.stream()
			.map(File::toPath)
			.map(m_navigator.getCurrentDir()::resolve)
			.map(m_dmService::addRunDependencies)
			.forEach(builder::addResult);
		
		return builder.build();
	}

}
