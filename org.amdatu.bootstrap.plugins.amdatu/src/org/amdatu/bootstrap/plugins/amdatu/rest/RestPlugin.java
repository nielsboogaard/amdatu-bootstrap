package org.amdatu.bootstrap.plugins.amdatu.rest;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import org.amdatu.bootstrap.core.BootstrapPlugin;
import org.amdatu.bootstrap.core.Command;
import org.amdatu.bootstrap.core.Dependency;
import org.amdatu.bootstrap.core.DependencyBuilder;
import org.amdatu.bootstrap.core.FullyQualifiedName;
import org.amdatu.bootstrap.core.InstallResult;
import org.amdatu.bootstrap.core.InstallResult.Builder;
import org.amdatu.bootstrap.core.Navigator;
import org.amdatu.bootstrap.core.PluginInfo;
import org.amdatu.bootstrap.getopt.Arguments;
import org.amdatu.bootstrap.getopt.Parameters;
import org.amdatu.bootstrap.plugins.dependencymanager.DmService;
import org.amdatu.template.processor.TemplateContext;
import org.amdatu.template.processor.TemplateEngine;
import org.amdatu.template.processor.TemplateException;
import org.amdatu.template.processor.TemplateProcessor;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.Inject;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.osgi.framework.BundleContext;

import aQute.lib.getopt.Description;

@Component
@PluginInfo(name="rest")
public class RestPlugin implements BootstrapPlugin{

	@ServiceDependency
	private volatile DependencyBuilder m_dependencyBuilder;
	
	@ServiceDependency
	private volatile DmService m_dmService;	
	
	@ServiceDependency
	private volatile Navigator m_navigator;
	
	@ServiceDependency
	private volatile TemplateEngine m_templateEngine;
	
	@Inject
	private volatile BundleContext m_bundleContext;
	
	@Command
	public InstallResult install() {
		return m_dependencyBuilder.addDependencies(
				Dependency.fromStrings(
						"org.amdatu.web.rest.jaxrs", 
						"org.amdatu.web.rest.doc", 
						"javax.servlet", 
						"com.fasterxml.jackson.core.jackson-annotations",
						"com.fasterxml.jackson.core.jackson-core",
						"com.fasterxml.jackson.core.jackson-databind"));
	}

	@Arguments(arg = "runconfig...")
	interface RestRunArguments extends Parameters {
		List<File> arguments();
	}
	
	@Command
	public InstallResult run(RestRunArguments args) {
		List<File> runConfigs = args.arguments();
		
		List<Dependency> deps = Dependency.fromStrings(
				"org.apache.felix.http.jetty",
				"org.apache.felix.http.api",
				"org.apache.felix.http.servlet-api",
				"org.apache.felix.http.whiteboard", 
				"org.amdatu.web.rest.jaxrs", 
				"org.amdatu.web.rest.wink",
				"org.amdatu.web.rest.doc", 
				"com.fasterxml.jackson.core.jackson-annotations",
				"com.fasterxml.jackson.core.jackson-core",
				"com.fasterxml.jackson.core.jackson-databind",
				"com.fasterxml.jackson.jaxrs.jackson-jaxrs-base",
				"com.fasterxml.jackson.jaxrs.jackson-jaxrs-json-provider"
				);
		
		Builder builder = InstallResult.builder();
		
		runConfigs.stream()
			.map(File::toPath)
			.map(m_navigator.getCurrentDir()::resolve)
			.map(p -> m_dependencyBuilder.addRunDependency(deps, p))
			.forEach(builder::addResult);
		
		runConfigs.stream()
			.map(File::toPath)
			.map(m_navigator.getCurrentDir()::resolve)
			.map(m_dmService::addRunDependencies)
			.forEach(builder::addResult);
		
		return builder.build();
	}

	interface CreateComponentArguments extends Parameters{
		FullyQualifiedName fqn();
		String path();
		boolean annotations();
	}
	
	@Command
	public File createcomponent(CreateComponentArguments args) {
		FullyQualifiedName fqn = args.fqn();
		String path = args.path();
		boolean useAnnotations = args.annotations();
		

		URL templateUri = m_bundleContext.getBundle().getEntry("/templates/restcomponent.vm");
		
		try {
			TemplateProcessor processor = m_templateEngine.createProcessor(templateUri);
			TemplateContext context = m_templateEngine.createContext();
			context.put("componentName", fqn.getClassName());
			context.put("packageName", fqn.getPackageName());
			context.put("useAnnotations", useAnnotations);
			context.put("path", path);

			String template = processor.generateString(context);
			Path outputDir = m_navigator.getCurrentDir().resolve("src")
					.resolve(fqn.getPackageName().replaceAll("\\.", "/"));
			Path outputFile = outputDir.resolve(fqn.getClassName() + ".java");
			Files.createDirectories(outputDir);
			
			Files.write(outputFile, template.getBytes());
			
			 if (useAnnotations) {
				 m_dmService.installAnnotationProcessor();
			 } else {
				 m_dmService.createActivator(fqn, new FullyQualifiedName("java.lang.Object"));
			 }
			 
			 m_dmService.addDependencies();
			
			return outputFile.toFile();
		} catch (TemplateException | IOException e) {
			throw new RuntimeException(e);
		}
	}	
	
}
