package org.amdatu.bootstrap.plugins.amdatu.jpa;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

import org.amdatu.bootstrap.core.BootstrapPlugin;
import org.amdatu.bootstrap.core.Command;
import org.amdatu.bootstrap.core.Dependency;
import org.amdatu.bootstrap.core.DependencyBuilder;
import org.amdatu.bootstrap.core.InstallResult;
import org.amdatu.bootstrap.core.InstallResult.Builder;
import org.amdatu.bootstrap.core.Navigator;
import org.amdatu.bootstrap.core.PluginInfo;
import org.amdatu.bootstrap.core.ResourceManager;
import org.amdatu.bootstrap.getopt.Arguments;
import org.amdatu.bootstrap.getopt.Description;
import org.amdatu.bootstrap.getopt.Parameters;
import org.amdatu.bootstrap.getopt.Select;
import org.amdatu.bootstrap.getopt.SelectOption;
import org.amdatu.bootstrap.plugins.dependencymanager.DmService;
import org.amdatu.template.processor.TemplateContext;
import org.amdatu.template.processor.TemplateEngine;
import org.amdatu.template.processor.TemplateException;
import org.amdatu.template.processor.TemplateProcessor;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.Inject;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.osgi.framework.BundleContext;

import aQute.bnd.build.model.BndEditModel;
import aQute.bnd.properties.Document;

@PluginInfo(name = "jpa")
@Component
public class JpaPlugin implements BootstrapPlugin {

	@ServiceDependency
	private volatile DependencyBuilder m_dependencyBuilder;

	@ServiceDependency
	private volatile DmService m_dmService;
	
	@ServiceDependency
	private volatile TemplateEngine m_templateEngine;
	
	@Inject
	private volatile BundleContext m_bundleContext;
	
	@ServiceDependency
	private volatile Navigator m_navigator;

	@ServiceDependency
	private volatile ResourceManager m_resourceManager;
	
	@Command
	public InstallResult install() {
		Builder builder = InstallResult.builder();

		builder.addResult(
				m_dependencyBuilder.addDependencies(
						Dependency.fromStrings(
								"org.amdatu.jta", 
								"javax.persistence")));

		return builder.build();
	}
	
	@Arguments(arg = "runfile")
	interface RunParams extends Parameters{
		@Description("Location of run configuration")
		List<File> arguments();
		
		@Description("Install a persistence provider")
		@Select({@SelectOption(value="hibernate", description="Use Hibernate persistence provider"),
					@SelectOption(value="openjpa", description="Use OpenJPA persistence provider"),
					@SelectOption(value="eclipselink", description="Use EclipdeLink persistence provider")})
		String provider();
		
	}
	
	@Command
	public List<InstallResult> run(RunParams params) {
		
		List<File> runFiles = params.arguments();
		
		runFiles.stream()
			.map(File::toPath)
			.map(m_navigator.getCurrentDir()::resolve)
			.forEach(this::addTxRunPath);
		
		return runFiles.stream()
				.map(File::toPath)
				.map(m_navigator.getCurrentDir()::resolve)
				.map(bndFile-> addRunDependencies(bndFile, params))
				.collect(Collectors.toList());
	}
	
	interface PersistenceUnitParams extends Parameters{
		@Description("persistence unit name")
		String puName();
		
		@Description("managed data source name")
		String managedDsName();
		
		@Description("unmanaged data source name")
		String unManagedDsName();
		
		@Description("provider name")
		String provider();
		
		@Description("bnd file to add persistence manafest headers")
		File bndfile();
	}
	
	
	@Command
	public File persistencexml(PersistenceUnitParams params) {
		URL templateUri = m_bundleContext.getBundle().getEntry(getTemplateUrl(params.provider()));
		
		Path bndFile = params.bndfile() != null ? m_navigator.getCurrentDir().resolve(params.bndfile().toPath()) : m_navigator.getBndFile();
		
		addManifestHeaders(bndFile);
		
		try {
			TemplateProcessor processor = m_templateEngine.createProcessor(templateUri);
			TemplateContext context = m_templateEngine.createContext();
			context.put("persistenceUnitName", params.puName());
			context.put("managedDsName", params.managedDsName());
			context.put("unmanagedDsName", params.unManagedDsName());

			String template = processor.generateString(context);
			Path outputDir = m_navigator.getCurrentDir().resolve("persistence");
					
			Path outputFile = outputDir.resolve("persistence.xml");
			Files.createDirectories(outputDir);
			
			Files.write(outputFile, template.getBytes());
			
			return outputFile.toFile();
		} catch (TemplateException | IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	private void addManifestHeaders(Path bndFile) {
		try {
			BndEditModel model = new BndEditModel();

			model.loadFrom(bndFile.toFile());

			model.addIncludeResource("META-INF/persistence.xml=persistence/persistence.xml");
			model.genericSet("Meta-Persistence", "META-INF/persistence.xml");

			String bndContents = new String(Files.readAllBytes(bndFile));
			Document document = new Document(bndContents);
			model.saveChangesTo(document);

			m_resourceManager.writeFile(bndFile, document.get().getBytes());
		} catch (IOException e) {
			throw new RuntimeException("Error writing manifest headers to bnd file", e);
		}

	}

	private String getTemplateUrl(String provider) {
		switch(provider) {
			case "hibernate": 
				return "/templates/hibernate-persistencexml.vm";
			case "eclipselink": 
				return "/templates/eclipselink-persistencexml.vm";
			case "openjpa": 
				return "/templates/openjpa-persistencexml.vm";
			default: 
				throw new RuntimeException("No persistence provider install in project");
		}
	}
	
	private InstallResult addRunDependencies(Path bndFile, RunParams params) {
		Builder builder = InstallResult.builder();

		builder.addResult(m_dependencyBuilder.addRunDependency(commonDependencies(), bndFile));
		
		if("hibernate".equals(params.provider())) {
			builder.addResult(m_dependencyBuilder.addRunDependency(hibernateDependencies(), bndFile));
		}
		
		if("openjpa".equals(params.provider())) {
			builder.addResult(m_dependencyBuilder.addRunDependency(openJpaDependencies(), bndFile));
		}
		
		if("eclipselink".equals(params.provider())) {
			builder.addResult(m_dependencyBuilder.addRunDependency(eclipseLinkDependencies(), bndFile));
		}

		builder.addResult(m_dmService.addRunDependencies(bndFile));
		return builder.build();
	}
	
	private List<Dependency> commonDependencies() {
		return Dependency.fromStrings(
				"org.amdatu.jta",
				"org.apache.servicemix.bundles.commons-dbcp",
				"javax.persistence;version='[2.0.3.v201010191057,2.0.3.v201010191057]'",
				"org.apache.commons.collections",
				"org.apache.commons.lang",
				"org.apache.commons.pool",
				"org.apache.aries.util",
				"org.apache.aries.transaction.manager",
				"org.apache.commons.lang3",
				"org.amdatu.jpa.datasourcefactory",
				"org.amdatu.jpa.extender",
				"org.amdatu.jta.manager",
				"slf4j.api",
				"org.amdatu.log.slf4jbridge");
	}
	
	private List<Dependency> hibernateDependencies() {
		return Dependency.fromStrings(
			"org.hibernate.core",
			"org.hibernate.entitymanager",
			"org.apache.servicemix.bundles.antlr",
			"classmate",
			"javassist",
			"org.apache.servicemix.bundles.dom4j",
			"org.hibernate.common.hibernate-commons-annotations",
			"org.jboss.logging.jboss-logging",
			"org.jboss.jandex;version=latest");
	}
	
	private List<Dependency> openJpaDependencies() {
		return Dependency.fromStrings(
			"org.apache.xbean.asm5-shaded",
			"org.apache.openjpa;version='[2.4.0,2.5.0)",
			"org.apache.servicemix.bundles.serp"
		);
	}
	
	private List<Dependency> eclipseLinkDependencies() {
		return Dependency.fromStrings(
				"org.eclipse.persistence.antlr",
				"org.eclipse.persistence.asm",
				"org.eclipse.persistence.core",
				"org.eclipse.persistence.jpa",
				"org.eclipse.persistence.jpa.jpql",
				"org.amdatu.jpa.adapter.eclipselink;version=latest"
		);
	}
	
	private void addTxRunPath(Path bndFile) {
		BndEditModel model = new BndEditModel();

		try {
			model.loadFrom(bndFile.toFile());
			String runPath = (String)model.genericGet("-runpath");
			if(runPath != null) {
				if(!runPath.contains("org.apache.geronimo.specs.geronimo-jta_1.1_spec")) {
					runPath = runPath + ","
							+ ".geronimo.specs.geronimo-jta_1.1_spec;version=1.1.1";
				}
			} else {
				runPath = "org.apache.geronimo.specs.geronimo-jta_1.1_spec;version=1.1.1";
			}
			
			model.genericSet("-runpath", runPath);
			
			String bndContents = new String(Files.readAllBytes(bndFile));
			Document document = new Document(bndContents);
			model.saveChangesTo(document);

			m_resourceManager.writeFile(bndFile, document.get().getBytes());
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		
	}
}
