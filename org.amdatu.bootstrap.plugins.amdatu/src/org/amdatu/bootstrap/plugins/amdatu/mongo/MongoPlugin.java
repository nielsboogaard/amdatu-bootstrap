package org.amdatu.bootstrap.plugins.amdatu.mongo;

import java.io.File;
import java.util.List;

import org.amdatu.bootstrap.core.BootstrapPlugin;
import org.amdatu.bootstrap.core.Command;
import org.amdatu.bootstrap.core.Dependency;
import org.amdatu.bootstrap.core.DependencyBuilder;
import org.amdatu.bootstrap.core.InstallResult;
import org.amdatu.bootstrap.core.InstallResult.Builder;
import org.amdatu.bootstrap.core.Navigator;
import org.amdatu.bootstrap.core.PluginInfo;
import org.amdatu.bootstrap.getopt.Arguments;
import org.amdatu.bootstrap.getopt.Description;
import org.amdatu.bootstrap.getopt.Parameters;
import org.amdatu.bootstrap.plugins.dependencymanager.DmService;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;


@Component
@PluginInfo(name="mongo")
public class MongoPlugin implements BootstrapPlugin{
	@ServiceDependency
	private volatile DependencyBuilder m_dependencyBuilder;

	@ServiceDependency
	private volatile DmService m_dmService;
	
	@ServiceDependency
	private volatile Navigator m_navigator;
	
	interface MongoInstallParameters extends Parameters {
		@Description("Install MongoJack")
		boolean mongojack();
		
		@Description("Install Jongo")
		boolean jongo();
	}

	@Command
	public InstallResult install(MongoInstallParameters params) {
		List<Dependency> dependencies = Dependency.fromStrings("org.mongodb.mongo-java-driver", "org.amdatu.mongo");

		if(params.mongojack()) {
			installMongoJack(dependencies);
		}
		
		if(params.jongo()) {
			installJongo(dependencies);
		}
		
		return m_dependencyBuilder.addDependencies(dependencies);
	}

	private void installJongo(List<Dependency> dependencies) {
		dependencies.addAll(Dependency.fromStrings(
				"com.fasterxml.jackson.core.jackson-core;version='[2.3.1,2.3.1]'",
				"com.fasterxml.jackson.core.jackson-databind;version='[2.3.1,2.3.1]'",
				"com.fasterxml.jackson.core.jackson-annotations;version='[2.3.0,2.3.0]'",
				"org.jongo", 
				"de.undercouch.bson4jackson"));
	}

	private void installMongoJack(List<Dependency> dependencies) {
		dependencies.addAll(Dependency.fromStrings(
				"com.fasterxml.jackson.core.jackson-core;version='[2.3.1,2.3.1]'",
				"com.fasterxml.jackson.core.jackson-databind;version='[2.3.1,2.3.1]'",
				"com.fasterxml.jackson.core.jackson-annotations;version='[2.3.0,2.3.0]'", 
				"org.mongojack;version=2.1.0.SNAPSHOT"));
	}
	
	@Arguments(arg = "runfile")
	interface MongoRunConfigParameters extends Parameters {
		@Description("Location of run configuration")
		List<File> arguments();
		
		@Description("Install MongoJack")
		boolean mongojack();
		
		@Description("Install Jongo")
		boolean jongo();
	}

	
	@Command
	public InstallResult run(MongoRunConfigParameters params) {

		List<File> runConfigs = params.arguments();
		
		Builder builder = InstallResult.builder();
		
		List<Dependency> dependencies = Dependency.fromStrings(
				"org.mongodb.mongo-java-driver", 
				"org.amdatu.mongo",
				"org.apache.felix.configadmin");
		
		if(params.mongojack()) {
			installMongoJack(dependencies);
		}
		
		if(params.jongo()) {
			installJongo(dependencies);
		}
		
		runConfigs.stream()
			.map(File::toPath)
			.map(m_navigator.getCurrentDir()::resolve)
			.map(p -> m_dependencyBuilder.addRunDependency(dependencies, p))
			.forEach(builder::addResult);
	
		runConfigs.stream()
			.map(File::toPath)
			.map(m_navigator.getCurrentDir()::resolve)
			.map(m_dmService::addRunDependencies)
			.forEach(builder::addResult);
		
		return builder.build();
	}
}
