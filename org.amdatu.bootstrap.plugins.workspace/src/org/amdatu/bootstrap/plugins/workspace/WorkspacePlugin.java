package org.amdatu.bootstrap.plugins.workspace;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.amdatu.bootstrap.core.BootstrapPlugin;
import org.amdatu.bootstrap.core.Command;
import org.amdatu.bootstrap.core.Feedback;
import org.amdatu.bootstrap.core.Navigator;
import org.amdatu.bootstrap.core.PluginInfo;
import org.amdatu.bootstrap.core.Prompt;
import org.amdatu.bootstrap.getopt.Arguments;
import org.amdatu.bootstrap.getopt.Description;
import org.amdatu.bootstrap.getopt.Parameters;
import org.amdatu.bootstrap.template.Template;
import org.amdatu.bootstrap.template.TemplateException;
import org.amdatu.bootstrap.template.TemplateProcessor;
import org.amdatu.bootstrap.template.TemplateProvider;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventAdmin;

@Component
@PluginInfo(name = "workspace")
public class WorkspacePlugin implements BootstrapPlugin {
	
	@ServiceDependency
	private volatile EventAdmin m_eventAdmin;
	
	@ServiceDependency
	private volatile Navigator m_navigator;
	
	@ServiceDependency
	private volatile Feedback m_feedback;
	
	@ServiceDependency
	private volatile TemplateProcessor m_templateProcessor;
	
	private final Set<TemplateProvider> m_workspaceTemplateProviders = new HashSet<>();
	
	interface InitParameters extends Parameters {
		
		String template();

	}
	
	@Command
	public File init(InitParameters parameters, Prompt prompt){
		if (m_navigator.getWorkspaceDir() != null){
			m_feedback.println("Can't init workspace (already in a workspace)");
			return null;
		}
		
		File file = m_navigator.getCurrentDir().toFile();
		
		try {
			Template template = determineTemplate(parameters, prompt, m_workspaceTemplateProviders);
			m_templateProcessor.installTemplate(template, file, new HashMap<String, Object>());
		} catch (TemplateException e) {
			throw new RuntimeException(e);
		}
				
		m_navigator.changeDir(file.toPath());
		
		
		for (File project : file.listFiles(new FileFilter() {
			
			@Override
			public boolean accept(File pathname) {
				return new File(pathname, ".project").exists();
			}
		})){
			sendEvent("org/amdatu/bootstrap/core/PROJECT_CREATED", project.getName());
		}
		
		return file;
	}
	
	@Arguments(arg="name")
	interface CreateParameters extends InitParameters {
		@Description("Name of the workspace")
		String[] arguments();
		
	}
	
	@Command
	public File create(CreateParameters parameters, Prompt prompt) {
		if (m_navigator.getWorkspaceDir() != null){
			m_feedback.println("Can't create workspace (already in a workspace)");
			return null;
		}
		
		String name = parameters.arguments()[0];
		
		File file = new File(m_navigator.getCurrentDir().toFile(), name);
		if (file.exists()){
			m_feedback.println("Can't create workspace (file exists)");
			return null;
		}
		
		if (!file.mkdir()){
			m_feedback.println("Can't create workspace (couldn't create workspace dir)");
			return null;
		}		
		
		try {
			Template template = determineTemplate(parameters, prompt, m_workspaceTemplateProviders);
			m_templateProcessor.installTemplate(template, file, new HashMap<String, Object>());
		} catch (TemplateException e) {
			throw new RuntimeException(e);
		}
				
		m_navigator.changeDir(file.toPath());
		return file;
	}

	
	

	private Template determineTemplate(InitParameters parameters, Prompt prompt, Set<TemplateProvider> templateProviders) {
		List<Template> templateOptions = new ArrayList<>();
		
		
		for (TemplateProvider p : templateProviders){
			templateOptions.addAll(p.listTemplates());
		}

		Template template = null;
		if (parameters.template() != null){
			for (Template t : templateOptions) {
				if (t.getName().equals(parameters.template())){
					template = t;
					break;
				}
			}
			if (template == null){
				m_feedback.println(String.format("Template '%s' not found", parameters.template()));
			}
			
		} else {
			if (templateOptions.size() == 0){
				m_feedback.println("No workspace template found");
			} else if (templateOptions.size() == 1){
				template = templateOptions.get(0);
			} else {
				template = prompt.askChoice("Which template do you want to use", 0, templateOptions);
			}
		}
		return template;
	}
		
	private void sendEvent(String topicName, String name) {
	    Map<String, Object> props = new HashMap<>();
	    props.put("projectname", name);
	    Event event = new Event(topicName, props);
	    m_eventAdmin.sendEvent(event);
	}
	
	@ServiceDependency(removed="workspaceTemplateProviderRemoved", filter="(type=workspace)")
	private void workspaceTemplateProviderAdded(TemplateProvider templateProvider){
		m_workspaceTemplateProviders.add(templateProvider);
	}
	
	@SuppressWarnings("unused" /* dependency manager callback */)
	private void workspaceTemplateProviderRemoved(TemplateProvider templateProvider) {
		m_workspaceTemplateProviders.remove(templateProvider);
	}
	
}
