package org.amdatu.bootstrap.plugins.workspace;

import org.amdatu.bootstrap.template.BundleTemplateProvider;
import org.amdatu.bootstrap.template.TemplateProvider;
import org.apache.felix.dm.annotation.api.BundleDependency;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.Property;
import org.osgi.framework.Bundle;

@Component(provides=TemplateProvider.class, properties=  @Property(name="type", value="workspace"))
public class WorkspaceBundleTemplateProvider extends BundleTemplateProvider {

	public static final String WORKSPACE_TEMPLATE_HEADER = "X-Bootstrap-WorkspaceTemplate";

	public static final String WORKSPACE_TEMPLATE_BUNDLE_FILTER = "(" + WORKSPACE_TEMPLATE_HEADER + "=*)";
	
	public WorkspaceBundleTemplateProvider() {
		super(WORKSPACE_TEMPLATE_HEADER);
	}
	
	@Override
	@BundleDependency(filter = WORKSPACE_TEMPLATE_BUNDLE_FILTER, removed="bundleRemoved")
	protected void bundleAdded(Bundle bundle) {
		super.bundleAdded(bundle);
	}
	
	@Override
	protected void bundleRemoved(Bundle bundle) {
		super.bundleRemoved(bundle);
	}

}
