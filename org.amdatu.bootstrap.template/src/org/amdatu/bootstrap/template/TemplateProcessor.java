package org.amdatu.bootstrap.template;

import java.io.File;
import java.util.Map;

public interface TemplateProcessor {

	void installTemplate(Template template, File file, Map<String, Object> properties) 
			throws TemplateException;

}
