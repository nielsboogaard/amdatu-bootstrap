package org.amdatu.bootstrap.template;

import java.util.ArrayList;
import java.util.List;

public class Template{
	
	private final String name; 
	
	private final List<TemplateResource> resources = new ArrayList<>();  
	
	public Template(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
	public List<TemplateResource> getResources() {
		return resources;
	}

	public void addResource(TemplateResource templateResource) {
		resources.add(templateResource);
	}

	@Override
	public String toString() {
		return name;
	}
		
}