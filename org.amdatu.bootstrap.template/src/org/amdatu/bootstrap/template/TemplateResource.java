package org.amdatu.bootstrap.template;

import java.net.URL;

public class TemplateResource {

	private URL url; 
	
	private String fileName;

	public TemplateResource(URL url, String fileName) {
		super();
		this.url = url;
		this.fileName = fileName;
	} 
	
	public URL getUrl() {
		return url;
	}

	public String getFileName() {
		return fileName;
	}
	
	
}
