/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.eclipse.plugin.services;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Properties;
import java.util.Scanner;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Path;
import org.eclipse.ui.IStartup;

public class ConnectorService implements IStartup {

    private static final String VERSION = "1.0.0";
    private static final int START = 29500;
    private static final int END = 29600;
    private Thread m_serverThread;
    private int m_port = -1;

    @Override
    public void earlyStartup() {
        startServer();
        IWorkspace workspace = ResourcesPlugin.getWorkspace();
        String workspaceDirectory = workspace.getRoot().getLocation().toFile().toString();
        workspaceDirectory += "/.metadata/";
        File settingsFile = new File(workspaceDirectory + "bootstrap.ini");
        Properties props = new Properties();
        props.put("version", VERSION);
        props.put("port", String.valueOf(m_port));
        OutputStream out;
        try {
            out = new FileOutputStream(settingsFile);
            props.store(out, null);
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private int startServer() {
        if (m_serverThread != null && m_serverThread.isAlive()) {
            return m_port;
        }
        final ServerSocket srvr = openSocket();

        // At this point if s is null we are helpless
        if (srvr == null) {
            m_port = -1;
            return m_port;
        }
        m_serverThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    boolean running = true;
                    while (running) {
                        Socket skt = srvr.accept();
    
                        try (Scanner scan = new Scanner(skt.getInputStream())) {
                            while (running && scan.hasNext()) {
                                String input = scan.nextLine();
                                if ("exit".equals(input)) {
                                    running = false;
                                } 
                                else if (input.startsWith("import")) {
                                    importProject(input);
                                } else if (input.startsWith("refresh")) {
                                    refreshProject(input);
                                } else {
                                    System.out.println("Unknown command: " + input);
                                }
                            }
                        }
    
                        skt.close();
                    }
                    srvr.close();
                }
                catch (Exception e) {

                }
            }
        });
        m_serverThread.setName("ServerThread");
        m_serverThread.start();
        m_port = srvr.getLocalPort();
        return m_port;
    }

    private ServerSocket openSocket() {
        ServerSocket srvr = null;

        for (int i = START; i < END; i++) {
            try {
                srvr = new ServerSocket(i);
                return srvr;
            }
            catch (IOException e) {
                // ignore we will try another port
            }
        }
        return srvr;
    }

    private void importProject(String command) {
        try {
            String[] split = command.split("\\s", 2);
            IProjectDescription description = ResourcesPlugin
                .getWorkspace().loadProjectDescription(new Path(split[1] + "/.project"));
            IProject project = ResourcesPlugin.getWorkspace()
                .getRoot().getProject(description.getName());
            project.create(description, null);
            project.open(null);
        }
        catch (CoreException e) {
            throw new RuntimeException(e);
        }
    }

    private void refreshProject(String command) {
        String[] split = command.split("\\s", 2);
        IProject[] projects = ResourcesPlugin.getWorkspace().getRoot().getProjects();
        for (IProject p : projects) {
            if (split[1].equals(p.getName())) {
                try {
                    p.refreshLocal(IProject.DEPTH_INFINITE, null);
                }
                catch (CoreException e) {
                    throw new RuntimeException(e);
                }
                // No need to loop any further as we are done
                return;
            }
        }
    }

}
