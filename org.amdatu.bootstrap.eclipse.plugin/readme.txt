You will need to have the Eclipse PDE installed in order to build this plugin.

You can install PDE by selecting the menu 'Help' -> 'Install New Software' then search for the 'PDE' plugin or
'Eclipse Plug-in Development Environment' and install these.