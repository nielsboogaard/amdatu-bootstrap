/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.eclipseconnector.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.nio.file.Path;
import java.util.Properties;

import org.amdatu.bootstrap.core.Navigator;
import org.amdatu.bootstrap.eclipseconnector.EclipseConnector;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.osgi.framework.Version;

@Component(provides = { EclipseConnector.class })
public class EclipseConnectorImpl implements EclipseConnector {

    private static final int COMPATIBILITY_MAJOR = 1;

    @ServiceDependency
    private volatile Navigator m_navigator;

    public void importProject(String name) {
        Path workspaceDir = m_navigator.getWorkspaceDir();
        sendCommand("import", workspaceDir + "/" + name);
    }

    public void refreshProject(String name) {
        sendCommand("refresh", name);
    }

    private void sendCommand(String command, String name) {
        Path workspaceDir = m_navigator.getWorkspaceDir();
        File bootstrapIni = new File(workspaceDir + "/.metadata/bootstrap.ini");
        if (bootstrapIni.exists()) {
            Properties props = new Properties();
            try {
                props.load(new FileInputStream(bootstrapIni));
            }
            catch (IOException e1) {
            }
            Version eclipsePluginVersion = new Version((String) props.get("version"));
            if (eclipsePluginVersion.getMajor() == COMPATIBILITY_MAJOR) {
                try (Socket socket = new Socket("localhost", Integer.parseInt((String) props.get("port")))) {
                    PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
                    out.println(command + " " + name);
                }
                catch (IOException e) {
                    System.out.println("Failed to communicate with eclipse");
                }
            }
        }
    }

}
