package org.amdatu.bootstrap.testutils.mockito.matchers;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

import org.amdatu.bootstrap.core.Dependency;
import org.mockito.ArgumentMatcher;

public class DependenciesMatcher extends ArgumentMatcher<Collection<Dependency>>{
	private final Set<String> m_expectedContains;
	
	public DependenciesMatcher(Set<String> expectedContains) {
		m_expectedContains = expectedContains;
	}
	
	@Override
	public boolean matches(Object argument) {
		@SuppressWarnings("unchecked")
		Collection<Dependency> deps = (Collection<Dependency>)argument;
		
		Set<String> bsns = deps.stream()
				.map(Dependency::getBsn)
				.collect(Collectors.toSet());
		
		return bsns.containsAll(m_expectedContains);
	}

}
