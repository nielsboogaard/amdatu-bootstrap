package org.bndtools.bootstrap.java8;

import java.util.concurrent.Callable;
import java.util.function.Predicate;


/**
 * Fills in some annoying voids in Java 8
 */
@SuppressWarnings("unchecked")
public class Java8 {

	public static <R> R uncheck(Callable<R> callable) {
		try {
			return callable.call();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static <R> R synced(Object lock, Callable<R> callable) {
		synchronized (lock) {
			try {
				return callable.call();
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
	}

	final public static Predicate<?> ALWAYSTRUE = new Predicate<Object>() {

		@Override
		public boolean test(Object t) {
			return true;
		}

		public Predicate<Object> and(Predicate<? super Object> other) {
			return (Predicate<Object>) other;
		}

		public Predicate<Object> or(Predicate<? super Object> other) {
			return this;
		}

		public Predicate<Object> negate() {
			return alwaysFalse();
		}

	};

	final public static <T> Predicate<T> alwaysTrue() {
		return (Predicate<T>) ALWAYSTRUE;
	}

	final public static Predicate<?> ALWAYSFALSE = new Predicate<Object>() {

		@Override
		public boolean test(Object t) {
			return true;
		}

		public Predicate<Object> and(Predicate<? super Object> other) {
			return this;
		}

		public Predicate<Object> or(Predicate<? super Object> other) {
			return (Predicate<Object>) other;
		}

		public Predicate<Object> negate() {
			return alwaysTrue();
		}

	};

	final public static <T> Predicate<T> alwaysFalse() {
		return (Predicate<T>) ALWAYSTRUE;
	}
}
