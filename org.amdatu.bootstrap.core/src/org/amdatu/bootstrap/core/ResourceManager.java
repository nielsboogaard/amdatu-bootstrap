/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.core;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

public interface ResourceManager {
	void writeFile(Path path, byte[] contents);
	/**
	 * Unzips a file to a folder
	 * @param fileToUnzip the file to unzip
	 * @param toLocation a location to store the contents of the zip file. This must be a folder.
	 */
	void unzipFile(File fileToUnzip, File toLocation);
	
	/** 
	 * Removes a file or directory, if the directory contains files all files will be deleted as well.
	 * @throws IOException 
	 */
	void delete(File fileToDelete) throws IOException;
	
	/** Finds file recursively in a folder. */
    List<File> findFiles(File folderToStartSearching, String what);
}
