package org.amdatu.bootstrap.core;

import rx.Observable;

public interface ObservablePrompt {
	Observable<String> askString(String description);
}
