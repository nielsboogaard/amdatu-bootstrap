package org.amdatu.bootstrap.core.commands;

import org.amdatu.bootstrap.core.BootstrapPlugin;
import org.amdatu.bootstrap.core.Feedback;
import org.amdatu.bootstrap.getopt.Arguments;
import org.amdatu.bootstrap.getopt.Parameters;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;


@Component
public class AnswerParserInteractiveCommand implements BootstrapPlugin{

	
	@ServiceDependency 
	private volatile Feedback m_feedback;
	
	@Arguments(arg = "interactive")
	interface InteractiveParameters extends Parameters {
		boolean arguments();
	}
	
	public void setInteractive(InteractiveParameters params) {
		//TODO: Set BootstrapHandler interactive flag
	}
}
