package org.amdatu.bootstrap.core.commands;

import static org.bndtools.bootstrap.java8.Java8.uncheck;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.amdatu.bootstrap.core.BootstrapPlugin;
import org.amdatu.bootstrap.core.Command;
import org.amdatu.bootstrap.core.Navigator;
import org.amdatu.bootstrap.core.PluginInfo;
import org.amdatu.bootstrap.getopt.Arguments;
import org.amdatu.bootstrap.getopt.Description;
import org.amdatu.bootstrap.getopt.Parameters;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.osgi.service.event.EventAdmin;

import aQute.libg.glob.Glob;

@Component
@PluginInfo(name = "navigation")
public class NavigationPlugin implements BootstrapPlugin {

	@ServiceDependency
	private volatile Navigator m_navigator;

	@ServiceDependency
	private volatile EventAdmin m_eventAdmin;
	
	@Arguments(arg = "dir")
	interface CdParameters extends Parameters {
		List<String> arguments();
	}
	
	@Command
	public File cd(CdParameters params) {
		List<String> arguments = params.arguments();
		String dirName = arguments.get(0);
		
		Path currentDir = m_navigator.getCurrentDir();
		Path newDir;
		
		if (dirName.equals("..")) {
			newDir = currentDir.getParent();
		}
		else if (dirName.equals("-")) {
			newDir = m_navigator.getPreviousDir();
		}
		else if (dirName.equals("~")) {
			newDir = m_navigator.getHomeDir();
		}
		else if (dirName.startsWith("~" + File.separator)) {
	        newDir = m_navigator.getHomeDir().resolve(dirName.substring(2));
		}
		else {
		    newDir = currentDir.resolve(dirName);
		}
		
		if (newDir.toFile().exists() && newDir.toFile().isDirectory()) {
			m_navigator.changeDir(newDir);
			return newDir.toFile();
		}
		else {
			throw new IllegalArgumentException("Invalid directory " + newDir);
		}
	}

	@Arguments(arg = "dir...")
	interface PwdParameters extends Parameters {}
	
	@Command
	public File pwd() {
		return m_navigator.getCurrentDir().toFile();
	}
	
	@Arguments(arg = "dir...")
	interface LsParameters extends Parameters {
		
		@Description("List of files to traverse, if not set use the default directory")
		List<File> arguments();

		@Description("Recurse into directories")
		boolean recursive();

		@Description("Only return files")
		boolean filesOnly();

		@Description("Filter on file name (last segment) with globbing")
		Glob match();

		@Description("Show all files (normally skips files starting with '.')")
		boolean all();

	}

	@Command
	@Description("Lists files in the given directories")
	public List<File> ls(LsParameters lsp) {
		try {
			List<File> args = lsp.arguments();
			if(args == null) {
				args = new ArrayList<>();
			}
			if (args.isEmpty()){
				args.add(m_navigator.getCurrentDir().toFile());
			}
			
			Stream<Path> stream = args.stream().map(File::toPath);

			int depth = lsp.recursive() ? Integer.MAX_VALUE : 1;
			stream = stream.flatMap(path -> uncheck(() -> Files.walk(path,
					depth)));

			if (!lsp.all())
				stream = stream.filter(p -> {
					return !p.getFileName().toString().startsWith(".");
				});
			
			
			Glob glob = lsp.match();
			if (glob != null)
				stream = stream.filter(path -> glob.matcher(path.getFileName().toString()).matches());


			Stream<File> files = stream.map(Path::toFile);

			if (lsp.filesOnly())
				files = files.filter(File::isFile);

			List<File> collect = files.collect(Collectors.toList());
			return collect;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	@Arguments(arg = "dir...")
	interface MkdirParams extends Parameters {
		@Description("List of dirs to create")
		List<File> arguments();
	}
	
	@Command
	@Description("Creates one or more directories")
	public void mkdir(MkdirParams params) {
		params.arguments().stream()
			.map(File::toPath)
			.map(p -> m_navigator.getCurrentDir().resolve(p))
			.map(Path::toFile)
			.forEach(File::mkdirs);
	}
}
