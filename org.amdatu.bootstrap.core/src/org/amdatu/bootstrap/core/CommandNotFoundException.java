package org.amdatu.bootstrap.core;


public class CommandNotFoundException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public CommandNotFoundException(String commandName) {
		super("Command '" + commandName + "' was not found");
	}
}
