package org.amdatu.bootstrap.core;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface Command {
	
	Scope scope() default Scope.GLOBAL;
	
}
