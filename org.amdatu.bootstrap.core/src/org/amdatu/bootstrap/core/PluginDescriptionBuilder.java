package org.amdatu.bootstrap.core;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.amdatu.bootstrap.getopt.Description;
import org.amdatu.bootstrap.getopt.Options;


public class PluginDescriptionBuilder {
	private String m_name;
	private List<CommandDescriptionBuilder> m_commands = new ArrayList<>(); 

	public PluginDescriptionBuilder(String name) {
		m_name = name;
	}
	
	public CommandDescriptionBuilder command(Method commandMethod) {
		CommandDescriptionBuilder commandBuilder = new CommandDescriptionBuilder(this, commandMethod);
		m_commands.add(commandBuilder);
		return commandBuilder;
	}
	
	public PluginDescription build() {
		List<CommandDescription> commands = m_commands.stream().map(CommandDescriptionBuilder::build).collect(Collectors.toList());
		return new PluginDescription(m_name, commands);
	}
	
	public static class CommandDescriptionBuilder {
		private PluginDescriptionBuilder m_pluginDescriptionBuilder;
		private String m_name;
		private String m_description;
		private List<CommandArgumentDescription> m_args = new ArrayList<>();
		
		public CommandDescriptionBuilder(PluginDescriptionBuilder pluginDescriptionBuilder, Method commandMethod) {
			m_pluginDescriptionBuilder = pluginDescriptionBuilder;
			m_name = commandMethod.getName();
			Arrays.stream(commandMethod.getParameters()).forEach(this::describeCommandParameters);
		}

		private void describeCommandParameters(Parameter arg) {
			if(Options.class.isAssignableFrom(arg.getType())) {
				Arrays.stream(arg.getType().getMethods()).forEach(m -> {
					Description description = m.getAnnotation(Description.class);
					String descriptionString = description != null ? description.value() : "";
					
					m_args.add(new CommandArgumentDescription(m.getName(), m.getReturnType().getName(), descriptionString));
				});
			} else {
				m_args.add(new CommandArgumentDescription(arg.getName(), arg.getType().getName(), null));
			}
		}
		
		public PluginDescriptionBuilder plugin() {
			return m_pluginDescriptionBuilder;
		}
		
		public CommandDescription build() {
			return new CommandDescription(m_name, m_description, m_args);
		}
	}
	
}
