/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.core;

public class FullyQualifiedName {
	private final String className;
	private final String packageName;
	
	public FullyQualifiedName(String name) {
		if(name.contains(".")) {
			packageName = name.substring(0, name.lastIndexOf("."));
			className = name.substring(name.lastIndexOf(".") + 1);
		} else {
			className = name;
			packageName = null;
		}
	}
	
	public String getClassName() {
		return className;
	}
	public String getPackageName() {
		return packageName;
	}

	public String getFull() {
		return packageName + "." + className;
	}

	@Override
	public String toString() {
		return getFull();
	}
}
