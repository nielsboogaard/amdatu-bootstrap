package org.amdatu.bootstrap.core;

import java.util.List;

public class CommandDescription {
	private final String m_name;
	private final String m_description;
	private final List<CommandArgumentDescription> m_arguments;

	public CommandDescription(String name, String description, List<CommandArgumentDescription> arguments) {
		m_name = name;
		m_description = description;
		m_arguments = arguments;
	}

	public String getName() {
		return m_name;
	}

	public String getDescription() {
		return m_description;
	}

	public List<CommandArgumentDescription> getArguments() {
		return m_arguments;
	}

}
