/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.core.impl;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.amdatu.bootstrap.core.Dependency;
import org.amdatu.bootstrap.core.DependencyBuilder;
import org.amdatu.bootstrap.core.Feedback;
import org.amdatu.bootstrap.core.InstallResult;
import org.amdatu.bootstrap.core.InstallResult.Builder;
import org.amdatu.bootstrap.core.Navigator;
import org.amdatu.bootstrap.core.ResourceManager;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventAdmin;

import aQute.bnd.build.model.BndEditModel;
import aQute.bnd.build.model.clauses.HeaderClause;
import aQute.bnd.build.model.clauses.VersionedClause;
import aQute.bnd.build.model.conversions.ClauseListConverter;
import aQute.bnd.build.model.conversions.CollectionFormatter;
import aQute.bnd.build.model.conversions.HeaderClauseFormatter;
import aQute.bnd.build.model.conversions.VersionedClauseConverter;
import aQute.bnd.header.Attrs;
import aQute.bnd.osgi.Macro;
import aQute.bnd.properties.Document;
import aQute.bnd.version.VersionRange;

import com.google.common.collect.Sets;

@Component
public class DependencyBuilderImpl implements DependencyBuilder {
	private final Set<String> skipParseVersion = Sets.newHashSet("latest", "file", "null");
	
	private final ClauseListConverter<VersionedClause> clauseListConverter  = new ClauseListConverter<>(new VersionedClauseConverter());;
	private final CollectionFormatter<HeaderClause> headerClauseListFormatter = new CollectionFormatter<HeaderClause>(BndEditModel.LIST_SEPARATOR, new HeaderClauseFormatter(), null);;
	
	@ServiceDependency
	private volatile Navigator m_navigator;
	
	@ServiceDependency
	private volatile ResourceManager m_resourceManager;
	
	@ServiceDependency
	private volatile EventAdmin m_eventAdmin;
	
	@ServiceDependency 
	private volatile Feedback m_feedback;
	
	@Override
	public InstallResult addDependency(String bsn) {
		return addDependency(bsn, null);
	}
	
	@Override
	public InstallResult addDependency(String bsn, String version) {
		return addDependency(new Dependency(bsn, version));
	}

	@Override
	public InstallResult addDependency(Dependency dependency) {
		Builder installResult = InstallResult.builder();
		
		try {
			BndEditModel model = new BndEditModel();
			Path bndFile = m_navigator.getBndFile();
			
			model.loadFrom(bndFile.toFile());
			
			List<VersionedClause> buildPath = model.getBuildPath();
			if(buildPath == null) {
				buildPath = new ArrayList<>();
			}
			
			boolean shouldBeAdded = true;
			for(VersionedClause versionedClause : buildPath) {
				if(versionedClause.getName().equals(dependency.getBsn())) {
					shouldBeAdded = false;
					break;
				}
			}
			
			if(shouldBeAdded) {
				Attrs attribs = new Attrs();
				
				if(dependency.getVersion() != null && !skipParseVersion.contains(dependency.getVersion())) {
					//Parse version and check validity
					new VersionRange(dependency.getVersion());
				}
				
				VersionedClause versionedClause = new VersionedClause(dependency.getBsn(), attribs);
				
				versionedClause.setVersionRange(dependency.getVersion());
				buildPath.add(versionedClause);

				model.setBuildPath(buildPath);
				
				Document document = new Document(new String(Files.readAllBytes(bndFile)));
				model.saveChangesTo(document);
				
				m_resourceManager.writeFile(bndFile, document.get().getBytes());
				
				installResult.addInstalled(dependency);
				sendEvent("org/amdatu/bootstrap/core/PROJECT_UPDATED", m_navigator.getCurrentProject().getName());
			} else {
				installResult.addSkipped(dependency);
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		
		return installResult.build();
	}
	
	@Override
    public InstallResult updateDependency(String bsn, String version) {
	    return updateDependency(new Dependency(bsn, version));
    }

	@Override
    public InstallResult updateDependency(String bsn) {
	    return updateDependency(bsn, null);
    }

    @Override
    public InstallResult updateDependency(Dependency d) {
        Dependency dependency;
        if ("null".equals(d.getVersion())) {
            dependency = new Dependency(d.getBsn(), null);
        } else {
            dependency = d;
        }
        
        // First try to update the dependency for the run bundles
        try {
            for (Path runFile : m_navigator.getBndRunFiles()) {
                BndEditModel model = new BndEditModel();
                model.loadFrom(runFile.toFile());
                
                updateRunVersion(dependency, model, null);
                
                Document document = new Document(new String(Files.readAllBytes(runFile)));
                model.saveChangesTo(document);
                m_resourceManager.writeFile(runFile, document.get().getBytes());
            }
        } catch (IOException | IllegalStateException e) {
            // do nothing we need to check for run files as well
        }
        
        try {
            BndEditModel model = new BndEditModel();
            Path bndFile = m_navigator.getBndFile();
            
            model.loadFrom(bndFile.toFile());
            
            List<VersionedClause> buildPath = model.getBuildPath();
            if(buildPath == null) {
                buildPath = new ArrayList<>();
            }
            
            if(dependency.getVersion() != null && !skipParseVersion.contains(dependency.getVersion())) {
                //Parse version and check validity
                new VersionRange(dependency.getVersion());
            }
           
            // find index
            int index = -1;
            for(VersionedClause versionedClause : buildPath) {
                if(versionedClause.getName().equals(dependency.getBsn())) {
                    index = buildPath.indexOf(versionedClause);
                    break;
                }
            }
            if (index > -1) {
                VersionedClause versionedClause = new VersionedClause(dependency.getBsn(), new Attrs());
                versionedClause.setVersionRange(dependency.getVersion());
                
                buildPath.set(index, versionedClause);
    
                model.setBuildPath(buildPath);
            }
            updateRunVersion(dependency, model, null);
            
            Document document = new Document(new String(Files.readAllBytes(bndFile)));
            model.saveChangesTo(document);
            
            m_resourceManager.writeFile(bndFile, document.get().getBytes());

            sendEvent("org/amdatu/bootstrap/core/PROJECT_UPDATED", m_navigator.getCurrentProject().getName());
            return InstallResult.builder().addUpdated(dependency).build();
        } catch (IOException | IllegalStateException e) {
            // do nothing
        }
        
        return InstallResult.builder().build();
    }

    private void updateRunVersion(Dependency dependency, BndEditModel model, String property) {
        // find index of dependency
        int index = -1;
        
        List<VersionedClause> runBundles;
        
        if (property == null){
        	runBundles = model.getRunBundles();
        }else{
        	Object object = model.genericGet(property);
			runBundles = clauseListConverter.convert((String)object);
        }
        
        if (runBundles != null) {
            for(VersionedClause versionedClause : runBundles) {
            	String propertyName;
            	if(versionedClause.getName().equals(dependency.getBsn())) {
                    index = runBundles.indexOf(versionedClause);
                    break;
                }else if ((propertyName = getPropertyName(versionedClause.getName())) != null){
                	updateRunVersion(dependency, model, propertyName);
                }
            }
        }
        
        if (index > -1) {
            VersionedClause versionedClause = new VersionedClause(dependency.getBsn(), new Attrs());
           
            if (dependency.getVersion() != null) {
                if ("latest".equals(dependency.getVersion())) {
                    versionedClause.setVersionRange(dependency.getVersion());
                } else {
                    // fix the version to exactly that version
                    versionedClause.setVersionRange("[" + dependency.getVersion() + "," + dependency.getVersion() +"]");
                }
            }
            
            runBundles.set(index, versionedClause);
            
            if (property == null){
				model.setRunBundles(runBundles);
            }else{
            	String convert = headerClauseListFormatter.convert(runBundles);
            	model.genericSet(property, convert);
            }
            m_feedback.println("Updated run bundle "+ dependency + " for project " + m_navigator.getCurrentDir().getFileName());
            sendEvent("org/amdatu/bootstrap/core/PROJECT_UPDATED", m_navigator.getCurrentProject().getName());
        }
    }

    @Override
	public InstallResult addDependencies(Collection<Dependency> dependencies) {
		Builder builder = InstallResult.builder();
    	
    	for (Dependency dependency : dependencies) {
			builder.addResult(addDependency(dependency));
		}
    	
    	return builder.build();
	}
	
	@Override
    public InstallResult addRunDependency(String bsn, Path runFile) {
		return addRunDependency(bsn, null);
    }

    @Override
    public InstallResult addRunDependency(String bsn, String version, Path runFile) {
        return addRunDependency(new Dependency(bsn, version), runFile);
    }

    @Override
    public InstallResult addRunDependency(Dependency dependency, Path runFile) {
        return addRunDependency(Arrays.asList(dependency), runFile);
    }

    @Override
    public InstallResult addRunDependency(Collection<Dependency> dependencies, Path runFile) {
    	Builder builder = InstallResult.builder();
    	
		try {
			BndEditModel model = new BndEditModel();
			model.loadFrom(runFile.toFile());
			for (Dependency dependency : dependencies) {
				// find index of dependency
				int index = -1;
				List<VersionedClause> runBundles = model.getRunBundles();
				if(runBundles == null) {
					runBundles = new ArrayList<>();
				}

				for (VersionedClause versionedClause : runBundles) {
					if (versionedClause.getName().equals(dependency.getBsn())) {
						index = runBundles.indexOf(versionedClause);
						break;
					}
				}

				VersionedClause versionedClause = new VersionedClause(dependency.getBsn(), new Attrs());
				if (dependency.getVersion() != null && !skipParseVersion.contains(dependency.getVersion())) {
					// fix the version to exactly that version
					if(!dependency.getVersion().contains("[")) {
						versionedClause.setVersionRange("[" + dependency.getVersion() + "," + dependency.getVersion() + "]");
					} else {
						versionedClause.setVersionRange(dependency.getVersion());
					}
				}
				if (index > -1) {
					runBundles.set(index, versionedClause);
					builder.addUpdated(dependency);
					
					//Project might be null for non-bnd projects
					if(m_navigator.getCurrentProject() != null) {
						sendEvent("org/amdatu/bootstrap/core/PROJECT_UPDATED", m_navigator.getCurrentProject().getName());
					}
				} else {
					runBundles.add(versionedClause);
					builder.addInstalled(dependency);
				}
				model.setRunBundles(runBundles);
			}
			Document document = new Document(new String(Files.readAllBytes(runFile)));
			model.saveChangesTo(document);
			m_resourceManager.writeFile(runFile, document.get().getBytes());
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
        
        return builder.build();
    }

    @Override
	public boolean hasDependency(Dependency dependency) {
		try {
			BndEditModel model = new BndEditModel();
			Path bndFile = m_navigator.getBndFile();
			model.loadFrom(bndFile.toFile());
			
			List<VersionedClause> buildpath = model.getBuildPath();
			if(buildpath == null) {
				return false;
			}
			
			for (VersionedClause versionedClause : buildpath) {
				if(versionedClause.getName().equals(dependency.getBsn())) {
					return true;
				}
			}
			
			return false;
			
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public boolean hasDependency(String bsn, String version) {
		return hasDependency(new Dependency(bsn, version));
	}

	@Override
	public boolean hasDependency(String bsn) {
		return hasDependency(new Dependency(bsn));
	}

	@Override
	public boolean hasDependencies(Collection<Dependency> dependencies) {
		for (Dependency dependency : dependencies) {
			if(!hasDependency(dependency)) {
				return false;
			}
		}
		
		return true;
	}
	
    @Override
    public List<Dependency> listDependencies() {
        List<Dependency> result = new ArrayList<>();
       
        try {
            BndEditModel model = new BndEditModel();
            Path bndFile = m_navigator.getBndFile();
            if (bndFile.toFile().exists()) {
                model.loadFrom(bndFile.toFile());
                
                List<VersionedClause> buildpath = model.getBuildPath();
                if(buildpath != null) {
                    
                for (VersionedClause versionedClause : buildpath) {
                    result.add(new Dependency(versionedClause.getName(), getRealVersion(versionedClause)));
                }
                
                if (model.getRunBundles() != null) {
                    // Also add run bundles of the current model
                    for (VersionedClause versionedClause : model.getRunBundles()) {
                        result.add(new Dependency(versionedClause.getName(), getRealVersion(versionedClause)));
                    }
                }
                }
            }
        } catch (IOException | IllegalStateException e) {
            // do nothing
        }
        try {
            // continue with run files
            for (Path runFile : m_navigator.getBndRunFiles()) {
                BndEditModel model = new BndEditModel();
                model.loadFrom(runFile.toFile());
                List<VersionedClause> runBundles = model.getRunBundles();
                
				result.addAll(parseDependencies(runBundles, model));
            }
        } catch (IOException | IllegalStateException e) {
            // do nothing
        }
            
        return result;
    }
    
    private List<Dependency> parseDependencies(List<VersionedClause> clauses, BndEditModel model){
    	List<Dependency> dependencies = new ArrayList<>();
    	
    	if (clauses == null){
    		return dependencies;
    	}
    	
    	for (VersionedClause versionedClause : clauses){
    		String propertyName = getPropertyName(versionedClause.getName());
    		if (propertyName != null){
        		Object object = model.genericGet(propertyName);
        		
        		ClauseListConverter<VersionedClause> converter = new ClauseListConverter<>(new VersionedClauseConverter());
        		List<VersionedClause> list = converter.convert((String)object);
        		
        		List<Dependency> parseVersionedClauseList = parseDependencies(list, model);
				dependencies.addAll(parseVersionedClauseList);
        	}else{
        		dependencies.add(new Dependency(versionedClause.getName(), getRealVersion(versionedClause)));
        	}	
    	}
    	
    	return dependencies;
    }
    
    private String getPropertyName(String clauseName){
    	if (clauseName.length() < 2 || !clauseName.startsWith("$")){
    		return null;
    	}
    	
    	if (clauseName.charAt(clauseName.length() -1 ) == Macro.getTerminator(clauseName.charAt(1))){
    		return clauseName.substring(2, clauseName.length() -1);
    	}
    	
    	return null;
    }

    private String getRealVersion(VersionedClause versionedClause) {
        String uVersion = versionedClause.getVersionRange();
        if (uVersion != null && uVersion.startsWith("[")) {
            // version is a range:
            VersionRange vr = new VersionRange(uVersion);
            uVersion = vr.getLow().toString();
        }
        return uVersion;
    }
    
    private void sendEvent(String topicName, String name) {
        Map<String, Object> props = new HashMap<>();
        props.put("projectname", name);
        Event event = new Event(topicName, props);
        m_eventAdmin.sendEvent(event);
    }
}	
