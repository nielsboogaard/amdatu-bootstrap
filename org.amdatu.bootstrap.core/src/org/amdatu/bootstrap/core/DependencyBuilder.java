/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.core;

import java.nio.file.Path;
import java.util.Collection;
import java.util.List;


public interface DependencyBuilder {

	InstallResult addDependency(String bsn);

	InstallResult addDependency(String bsn, String version);

	/** This will add a Dependency to the bnd build path of the current project. */
	InstallResult addDependency(Dependency dependency);

	InstallResult addDependencies(Collection<Dependency> dependencies);
	
	/** Will call {@link #addRunDependency(String, String)} with version null */
	InstallResult addRunDependency(String bsn, Path runFile);

	/** Will call {@link #addRunDependency(Dependency)} with a Dependency constructed from the bsn and version. */
	InstallResult addRunDependency(String bsn, String version, Path runFile);

    /** Will call {@link #addRunDependency(Collection)} with a list of 1 {@link Dependency}.  */
	InstallResult addRunDependency(Dependency dependency, Path runFile);

    /**
     * This will add a (list of) {@link Dependency} to the bnd run bundles of the current project. 
     * It will update if the {@link Dependency} already exits. 
     */
	InstallResult addRunDependency(Collection<Dependency> dependencies, Path runFile);

	boolean hasDependency(Dependency dependency);

	boolean hasDependency(String bsn, String version);

	boolean hasDependency(String bsn);
	
	boolean hasDependencies(Collection<Dependency> dependencies);
	
	/** This will return all build and run Dependencies of the current project. */
	List<Dependency> listDependencies();

	InstallResult updateDependency(String bsn);
	
	InstallResult updateDependency(String bsn, String version);
    
    /** This will update a build and run {@link Dependency} of the current project. */
	InstallResult updateDependency(Dependency dependency);
}
