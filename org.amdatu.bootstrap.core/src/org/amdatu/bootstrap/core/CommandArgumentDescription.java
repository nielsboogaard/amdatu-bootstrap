package org.amdatu.bootstrap.core;

public class CommandArgumentDescription {
	private final String m_name;
	private final String m_type;
	private final String m_description;

	public CommandArgumentDescription(String name, String type, String description) {
		m_name = name;
		m_type = type;
		m_description = description;
	}

	public String getName() {
		return m_name;
	}

	public String getType() {
		return m_type;
	}

	public String getDescription() {
		return m_description;
	}
}
