package org.amdatu.bootstrap.core;

import java.util.List;

public interface PluginRegistry {
	List<PluginDescription> listPlugins(); 
}
