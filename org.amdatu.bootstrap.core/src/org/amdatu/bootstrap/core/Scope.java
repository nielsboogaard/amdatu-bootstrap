package org.amdatu.bootstrap.core;

public enum Scope {
	GLOBAL, WORKSPACE, PROJECT
}
