package org.amdatu.bootstrap.core;

import java.util.List;

public interface Prompt {
	boolean askBoolean(String message, boolean defaultChoice);

	String askString(String message);

	String askString(String message, String defaultString);

	<T> T askChoice(String message, int defaultOption, List<T> options);

	int askChoiceAsIndex(String message, int defaultOption,List<? extends Object> options);

	FullyQualifiedName askComponentName();

	FullyQualifiedName askComponentName(String message);

	FullyQualifiedName askComponentName(String message, String defaultName);

}
