package org.amdatu.bootstrap.getopt;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
public @interface Description {
	String value();
}
