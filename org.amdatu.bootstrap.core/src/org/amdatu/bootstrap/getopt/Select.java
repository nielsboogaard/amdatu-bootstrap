package org.amdatu.bootstrap.getopt;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;


@Retention(RetentionPolicy.RUNTIME)
public @interface Select {

	SelectOption[] value();
	
}
