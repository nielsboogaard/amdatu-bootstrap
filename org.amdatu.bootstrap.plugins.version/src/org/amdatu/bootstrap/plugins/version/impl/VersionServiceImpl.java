/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.plugins.version.impl;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.amdatu.bootstrap.core.Dependency;
import org.amdatu.bootstrap.core.DependencyBuilder;
import org.amdatu.bootstrap.core.Feedback;
import org.amdatu.bootstrap.core.Navigator;
import org.amdatu.bootstrap.plugins.version.VersionService;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventAdmin;

import aQute.bnd.service.RepositoryPlugin;
import aQute.bnd.version.Version;

@Component(provides = org.amdatu.bootstrap.plugins.version.VersionService.class)
public class VersionServiceImpl implements VersionService {

    @ServiceDependency
    private volatile Navigator m_navigator;

    @ServiceDependency
    private volatile DependencyBuilder m_dependencyBuilder;

    @ServiceDependency
    private volatile EventAdmin m_eventAdmin;
    
    @ServiceDependency 
	private volatile Feedback m_feedback;

    @Override
    public SortedSet<Version> findVersion(String bsn) throws Exception {
        if (bsn == null) {
            // option was not valid exit..
            return new TreeSet<>();
        }
        List<RepositoryPlugin> repositories = getRepositories();
        if (repositories == null) {
            // Short circuit and exit we can't do anything
        	m_feedback.println("No repositories found");
        	return null;
        }
        SortedSet<Version> versions = new TreeSet<>();
        for (RepositoryPlugin r: repositories) {
            SortedSet<Version> sortedSet = r.versions(bsn);
            versions.addAll(sortedSet);
        }
        return versions;
    }

    @Override
    public List<RepositoryPlugin> getRepositories(){
        Path workspaceDir = m_navigator.getWorkspaceDir();
        
        if (workspaceDir == null) {
            throw new IllegalStateException("Need to be in a workspace or project to execute this");
        }
                
        List<RepositoryPlugin> repos = m_navigator.getCurrentWorkspace().getRepositories();
        return repos;
    }

    @Override
    public Map<String, Set<String>> getAllDependencies() {
        sendEvent("org/amdatu/bootstrap/core/BE_QUIET");
        Path workspaceDir = m_navigator.getWorkspaceDir();
        if (workspaceDir == null) {
        	m_feedback.println("Need to be in a workspace or project to execute this");
            return new HashMap<>();
        }

        Path currentDir = m_navigator.getCurrentDir();

        Path projectDir = m_navigator.getProjectDir();
        List<File> projects = new ArrayList<>();
        if (projectDir == null && !m_navigator.getBndRunFiles().isEmpty()) {
            projects.add(m_navigator.getCurrentDir().toFile());
        } else if (projectDir == null) {
            for (File f : workspaceDir.toFile().listFiles()) {
                if (f.isDirectory()) {
                    projects.add(f);
                }
            }
        } else {
            projects.add(projectDir.toFile());
            currentDir = projectDir;
        }

        Map<String, Set<String>> allDeps = new HashMap<>();

        for (File f : projects) {
            try {
                changeToDir(f.toPath());

                List<Dependency> list = m_dependencyBuilder.listDependencies();
                for (Dependency d : list) {
                    Set<String> versions = allDeps.get(d.getBsn());
                    if (versions == null) {
                        versions = new HashSet<>();
                        allDeps.put(d.getBsn(), versions);
                    }
                    versions.add(d.getVersion());
                }
            } catch (RuntimeException e) {
                // Probably not a correct folder ignore
            }
        }

        // reset to old dir
        changeToDir(currentDir);
        sendEvent("org/amdatu/bootstrap/core/RESET_QUIET");
        return allDeps;
    }

    @Override
    public File downloadVersion(String bsn, String version) {
        List<RepositoryPlugin> repositories = getRepositories();
        Version v = new Version(version);
        for (RepositoryPlugin r: repositories) {
            try {
                SortedSet<Version> sortedSet = r.versions(bsn);
                if (doContain(sortedSet, v)) {
                    return r.get(bsn, v, new HashMap<String, String>());
                }
            } catch (Exception e) {
            	m_feedback.println("Failed to get " + bsn + " with version " + version + " from repository " + r.getName());
            }
        }
        return null;
    }

    @Override
    public void setRepositoryFile(File repoFile) {
        try {
			Files.copy(repoFile.toPath(), m_navigator.getWorkspaceDir().resolve("cnf/ext/repositories.bnd"),
					StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			throw new RuntimeException("Failed to change repositories");
		}

        m_navigator.getCurrentWorkspace().refresh();

        m_feedback.println("Changed repository to " + repoFile);
    }

    private boolean doContain(SortedSet<Version> sortedSet, Version v) {
        for (Version version : sortedSet) {
            if (v.toString().equals(version.toString())) {
                return true;
            }
        }
        return false;
    }

    private void changeToDir(Path to) {
        Path currentDir = m_navigator.getCurrentDir();
        if (!currentDir.equals(to)) {
            m_navigator.changeDir(to);
        }
    }

    private void sendEvent(String topicName) {
        Map<String, Object> props = new HashMap<>();
        Event event = new Event(topicName, props);
        m_eventAdmin.sendEvent(event);
    }

}
