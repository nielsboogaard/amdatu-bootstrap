package org.amdatu.bootstrap.plugins.version.impl;

import java.io.File;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedSet;

import org.amdatu.bootstrap.core.BootstrapPlugin;
import org.amdatu.bootstrap.core.Command;
import org.amdatu.bootstrap.core.DependencyBuilder;
import org.amdatu.bootstrap.core.Feedback;
import org.amdatu.bootstrap.core.Navigator;
import org.amdatu.bootstrap.core.PluginInfo;
import org.amdatu.bootstrap.core.Prompt;
import org.amdatu.bootstrap.core.Scope;
import org.amdatu.bootstrap.getopt.Arguments;
import org.amdatu.bootstrap.getopt.Parameters;
import org.amdatu.bootstrap.plugins.version.VersionService;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventAdmin;

import aQute.bnd.build.Workspace;
import aQute.bnd.service.RepositoryPlugin;
import aQute.bnd.version.Version;

@Component
@PluginInfo(name="version")
public class VersionPlugin implements BootstrapPlugin {

	@ServiceDependency
	private volatile EventAdmin m_eventAdmin;
	
	@ServiceDependency
	private volatile DependencyBuilder m_dependencyBuilder;

	@ServiceDependency
	private volatile Navigator m_navigator;
	
	@ServiceDependency
	private volatile VersionService m_versionService;

	@ServiceDependency 
	private volatile Feedback m_feedback;
	
	@Arguments(arg="bsn")
	interface ListVersionsParameters extends Parameters {
		
		String[] arguments();
	}
	
	@Command(scope=Scope.WORKSPACE)
	public List<Version> list(ListVersionsParameters lvParams) {
		
		String bsn = lvParams.arguments()[0];
		
		SortedSet<Version> versions = new TreeSet<>();
		Workspace bndWorkspace = m_navigator.getCurrentWorkspace();
		for (RepositoryPlugin repositoryPlugin : bndWorkspace.getRepositories()) {
			try {
				versions.addAll(repositoryPlugin.versions(bsn));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
		return new ArrayList<Version>(versions);
	}

	@Arguments(arg="[bsn]")
	interface UpgradeVersionParameters extends Parameters{
		
		String[] arguments();
		
		String filter();
		
		boolean autoUpgrade();
		
	}
	
	@Command(scope=Scope.WORKSPACE)
	public Void upgrade(UpgradeVersionParameters params, Prompt prompt) {
		try {
			String bsn = params.arguments().length > 0?params.arguments()[0]:null;
			if (StringUtils.isNotEmpty(bsn)){
				upgradeVersion(bsn, prompt);
			}else{
				Map<String, Set<String>> allDeps = m_versionService.getAllDependencies();
		        if (allDeps.isEmpty()) {
		            // Nothing more to do... as there are no dependencies
		            return null	;
		        }
		        		
		        String bsnFiter = params.filter();
		        if (StringUtils.isNotEmpty(bsnFiter)) {
		            // apply filter over deps
		            Map<String, Set<String>> allDepsFilterd = new HashMap<>();
		            for (Entry<String, Set<String>> e : allDeps.entrySet()) {
		                if (e.getKey().contains(bsnFiter)) {
		                    allDepsFilterd.put(e.getKey(), e.getValue());
		                }
		            }
		            allDeps = allDepsFilterd;
		        }
		        
		        Boolean isInteractive = prompt!= null;
		        Boolean isUpgradeAll = params.autoUpgrade();
		        
		        boolean displayList = isInteractive || (!isInteractive && !isUpgradeAll);
		        List<String> upgradeble = listAllUpgradableDependencies(allDeps, displayList);
		        if (isInteractive) {
		            String lastUpdate = "";
		            while (lastUpdate != null && upgradeble.size() > 0) {
		                if (isUpgradeAll) {
		                	// just upgrade to the last version
		                	lastUpdate = upgradeVersionToLast(upgradeble.get(0));
		                } else {
		                	lastUpdate = upgradeVersion(upgradeble, prompt);
		                }
		                upgradeble.remove(lastUpdate);
		                if (!"".equals(lastUpdate) && lastUpdate != null) {
		                    if (!isUpgradeAll) {
		                        
		                        Boolean another = prompt.askBoolean("Upgrade another? ", true);
		                        if (!another) {
		                            return null;
		                        }
		                    }
		                }
		            }
		        }
			}
		}catch (Exception e){
			e.printStackTrace();
		}
        return null;
	}
	
    public String upgradeVersion(List<String> bsns, Prompt prompt) throws Exception {
    	String choice = prompt.askChoice("What bsn do you want to upgrade?", 0, bsns);
        
        if (choice == null) {
            return null;
        }
        return upgradeVersion(choice, prompt);
    }
	
    public String upgradeVersion(String bsn, Prompt prompt) throws Exception {
        SortedSet<Version> findVersion = m_versionService.findVersion(bsn);
        List<String> options = new ArrayList<>();
        options.add("null");
        options.add("latest");
        for (Version v : findVersion) {
            options.add(v.toString());
        }
        
        String toVersion = prompt.askChoice(
        		"To what version do you want to upgrade '" + bsn + "' to?", options.size() -1, options);
       
        upgradeVersion(bsn, toVersion);
        return bsn;
    }
    
    private String upgradeVersionToLast(String bsn) throws Exception {
        SortedSet<Version> findVersion = m_versionService.findVersion(bsn);
        upgradeVersion(bsn,findVersion.last().toString());
        return bsn;
    }

    private void upgradeVersion(String bsn, String toVersion) {
        if (bsn == null || toVersion == null) {
            // do nothing as bsn and version must be set
            return;
        }
        m_feedback.println("Upgrading " + bsn + " to " + toVersion);
        
        Path projectDir = m_navigator.getProjectDir();
        if (projectDir == null && !m_navigator.getBndRunFiles().isEmpty()) {
            m_dependencyBuilder.updateDependency(bsn, toVersion);
        } else if (projectDir != null) {
            m_dependencyBuilder.updateDependency(bsn, toVersion);
        } else {
            
            List<File> projects = new ArrayList<>();
            for (File f : m_navigator.getWorkspaceDir().toFile().listFiles()) {
                if (f.isDirectory()) {
                    projects.add(f);
                }
            }
            for (File f : projects) {
                try {
                    changeDir(f);
                    Map<String, Set<String>> dependencies = m_versionService.getAllDependencies();
                    if (dependencies.containsKey(bsn)) {
                        m_dependencyBuilder.updateDependency(bsn, toVersion);
                    }
                } catch (RuntimeException e) {
                    // Probably not a correct folder ignore
                }
            }
            // back to workspace
            changeDir(m_navigator.getWorkspaceDir());
        }
    }
    
	private List<String> listAllUpgradableDependencies(Map<String, Set<String>> allDeps, boolean displayList) throws Exception {
        // init the repositories, this way any messages will be at the front off the log
        m_versionService.getRepositories();

        // first we need the max length of the longest bundle name;
        int maxLength = getMaxLengthOfName(allDeps);
        List<String> upgradeble = new ArrayList<>();
        if (allDeps.isEmpty()) {
        	m_feedback.println("Nothing found");
            return upgradeble; 
        }
        // Print out table header
        if (displayList){
        	System.out.format("%" + (maxLength + 4) + "s%25s%25s%5s\n", "Name:", "Used:", "Available:",  "");
        }
        for (Entry<String, Set<String>> d : allDeps.entrySet()) {
            
            SortedSet<Version> versions = m_versionService.findVersion(d.getKey());
            for (String s : sortVersions(d.getValue())) {
                String name = "";
                String version = "";
                if (!versions.isEmpty()) {
                    version = versions.last().toString();
                }

                boolean isCurrent = isCurrentVersion(versions, s, version);
                if (!isCurrent) {
                	if (displayList) {
                		name = upgradeble.contains(d.getKey())?"":d.getKey();
                		System.out.format("%" + (maxLength + 4) + "s%25s%25s%5s\n", 
                				name, 
                				s, 
                				upgradeble.contains(d.getKey())?"":version, 
                				!isCurrent ? " <-- upgrade" : "");
                		
                	}
                	
                    if (!upgradeble.contains(d.getKey())){
                    	upgradeble.add(d.getKey());
                    }
                }
            }
        }
        return upgradeble;
    }
	
    private boolean isCurrentVersion(SortedSet<Version> versions, String usedVersion, String otherVersion) {
        if ("".equals(otherVersion) || "latest".equals(usedVersion)) {
            // not current as repo version is empty (could also be second line)
            // "latest" is a workspace version so thats ok 
            return true;
        }
        if (usedVersion != null && !versions.isEmpty()) {
            try {
                org.osgi.framework.Version v1 = new org.osgi.framework.Version(usedVersion);
                org.osgi.framework.Version v2 = new org.osgi.framework.Version(versions.last().toString());
                
                if (v1.compareTo(v2) >= 0) {
                    return true;
                }
            } catch (IllegalArgumentException e) {
                // not current
            }
        }
        
        return false;
    }

    private int getMaxLengthOfName(Map<String, Set<String>> allDeps) {
        int maxLength = 0;
        for (String name : allDeps.keySet()) {
            if (name.length() > maxLength) {
                maxLength = name.length();
            }
        }
        return maxLength;
    }
    
    private List<String> sortVersions(Set<String> set){
        List<String> result = new ArrayList<>(set);
        Comparator<String> versionComparator = new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                if (o1 == null && o2 == null) {
                    // both are equally null thus the same
                    return 0;
                }
                if (o1 == null) {
                    // something is better then null
                    return 1;
                }
                if (o2 == null) {
                    // something is better then null
                    return -1;
                }
                try {
                    org.osgi.framework.Version v1 = new org.osgi.framework.Version(o1);
                    org.osgi.framework.Version v2 = new org.osgi.framework.Version(o2);
                    return v1.compareTo(v2);
                } catch (IllegalArgumentException  e) {
                    // one of the versions was not a correct version number, we will just compare strings
                    return o1.compareTo(o2);
                }
            }
        };
        Collections.sort(result, versionComparator);
        return result;
    }
    
    private void changeDir(File f) {
        changeDir(f.toPath());
    }

    private void changeDir(Path p) {
        sendEvent("org/amdatu/bootstrap/core/BE_QUIET");
        m_navigator.changeDir(p);
        sendEvent("org/amdatu/bootstrap/core/RESET_QUIET");
    }

    private void sendEvent(String topicName) {
        Map<String, Object> props = new HashMap<>();
        Event event = new Event(topicName, props);
        m_eventAdmin.sendEvent(event);
    }
}
