package org.amdatu.bootstrap.gogo;

import org.amdatu.bootstrap.core.Feedback;
import org.apache.felix.dm.annotation.api.Component;

@Component
public class ShellFeedback implements Feedback{

	@Override
	public void println(String message) {
		System.out.println(message);
	}

}
