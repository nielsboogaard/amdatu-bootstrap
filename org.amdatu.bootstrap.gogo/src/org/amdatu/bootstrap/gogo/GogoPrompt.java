/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.gogo;

import java.util.List;
import java.util.Scanner;

import org.amdatu.bootstrap.core.FullyQualifiedName;
import org.amdatu.bootstrap.core.Prompt;
import org.apache.felix.service.command.CommandSession;

	
public class GogoPrompt implements Prompt {
		
	private volatile CommandSession m_session;
		
	public GogoPrompt(CommandSession session) {
		m_session = session;
	}

	@Override
	public boolean askBoolean(String message, boolean defaultChoice) {
		if(defaultChoice) {
			m_session.getConsole().println(message + ": [Y/n]");
		} else {
			m_session.getConsole().println(message + ": [y/N]");
		}
		
		//Can't close scanner here, or Gogo will crash
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(m_session.getKeyboard());
		
		String answer = scanner.nextLine();
		if(answer != null && answer.length() > 0) {
			return answer.equalsIgnoreCase("y");
		} else {
			return defaultChoice;
		}
	}

	@Override
	public String askString(String message) {
		return askString(message, null);
	}

	@Override
	public String askString(String message, String defaultString) {
		if(defaultString != null) {
			m_session.getConsole().println(message + ": [" + defaultString +"]");
		} else {
			m_session.getConsole().println(message + ": ");
		}
		
		//Can't close scanner here, or Gogo will crash
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(m_session.getKeyboard());
		
		String answer = scanner.nextLine();

		if(answer != null && (answer = answer.replaceAll("\0", "")).length() > 0) {
			return answer;
		} else if(defaultString != null) {
			return defaultString;
		} else {
			return askString(message);
		}
	}


	@Override
	public <T> T askChoice(String message, int defaultOption, List<T> options) {
		int askChoiceAsIndex = askChoiceAsIndex(message, defaultOption, options);
		if (askChoiceAsIndex >= 0) {
		    return options.get(askChoiceAsIndex);
		}
		// not a valid option
		return null;
	}
	
	@Override
	public int askChoiceAsIndex(String message, int defaultOption, List<? extends Object> options) {
		if(options.size() == 0) {
			throw new IllegalArgumentException("No options specified");
		}
		
		m_session.getConsole().println(message + ":");
		
		for(int i=0; i < options.size(); i++) {
			if(defaultOption == i) {
				m_session.getConsole().println(String.format("[%d]* - %s", i, options.get(i)));
			} else {
				m_session.getConsole().println(String.format("[%d] - %s", i, options.get(i)));
			}
		}
		
		//Can't close scanner here, or Gogo will crash
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(m_session.getKeyboard());
		
		String answer = scanner.nextLine();
		try {
			int answerInt = Integer.parseInt(answer);
			if(answerInt < options.size()) {
				return answerInt;
			}
		} catch(NumberFormatException e) {
			//Recover by returning default option
		}
		
		return defaultOption;
	}

	@Override
	public FullyQualifiedName askComponentName() {
		return askComponentName("What should be the name of the component?", "example.Example");
	}

	@Override
	public FullyQualifiedName askComponentName(String message) {
		return askComponentName(message, "example.Example");
	}

	@Override
	public FullyQualifiedName askComponentName(String message, String defaultName) {
		String name = askString(message, defaultName);
		
		return new FullyQualifiedName(name);
	}
}