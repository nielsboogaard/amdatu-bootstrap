package org.amdatu.bootstrap.gogo;

import java.io.File;

import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.service.command.Converter;

@Component
public class Converters implements Converter {

	@Override
	public Object convert(Class<?> desiredType, final Object in) throws Exception {
		return null;
	}

	@Override
	public CharSequence format(Object target, int level, Converter converter) throws Exception {
		switch (level) {
			case INSPECT:
				if (target instanceof File) {
					return ((File) target).getPath();
				}
				break;
			case LINE:
				if (target instanceof File) {
					return ((File) target).getPath();
				}
				break;
			case PART:
				if (target instanceof File) {
					return ((File) target).getPath();
				}
				break;
		}
		return null;
	}
}
