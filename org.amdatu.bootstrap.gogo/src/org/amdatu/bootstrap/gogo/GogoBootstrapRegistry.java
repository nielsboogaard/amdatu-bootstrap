package org.amdatu.bootstrap.gogo;

import static java.util.Arrays.stream;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.amdatu.bootstrap.core.BootstrapPlugin;
import org.amdatu.bootstrap.core.Command;
import org.amdatu.bootstrap.core.Navigator;
import org.amdatu.bootstrap.core.PluginDescription;
import org.amdatu.bootstrap.core.PluginDescriptionBuilder;
import org.amdatu.bootstrap.core.PluginInfo;
import org.amdatu.bootstrap.core.PluginRegistry;
import org.amdatu.bootstrap.core.Prompt;
import org.amdatu.bootstrap.core.Scope;
import org.amdatu.bootstrap.getopt.CommandLine;
import org.apache.felix.dm.DependencyManager;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.Inject;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.apache.felix.service.command.CommandProcessor;
import org.apache.felix.service.command.CommandSession;
import org.osgi.framework.BundleContext;

import aQute.lib.collections.ExtList;
import aQute.libg.reporter.ReporterAdapter;

@Component
public class GogoBootstrapRegistry implements PluginRegistry{
	
	@Inject
	private volatile BundleContext m_bundleContext;
	
	@Inject
	private volatile DependencyManager m_dm;
	
	private final Map<String, BootstrapPlugin> m_commands = new ConcurrentHashMap<>();

	private final Map<String, String> m_coreAliases = new HashMap<>();

	private org.apache.felix.dm.Component m_bootstrapHandlerComponent;
	
	public GogoBootstrapRegistry() {
		m_coreAliases.put("ls", "navigation-ls");
		m_coreAliases.put("pwd", "navigation-pwd");
		m_coreAliases.put("cd", "navigation-cd");
		m_coreAliases.put("mkdir", "navigation-mkdir");
	}
	
	@ServiceDependency(removed="removeCommand")
	public void addCommand(BootstrapPlugin plugin) throws Exception {
		getCommands(plugin).forEach(c -> m_commands.put(c, plugin));	
		m_commands.put(getPluginName(plugin).toLowerCase()+"-help", plugin);
		update();
	}
	
	public void removeCommand(BootstrapPlugin plugin) {
		getCommands(plugin).forEach(c -> m_commands.remove(c));
		m_commands.remove(getPluginName(plugin)+"-help");
		update();
	}
	
	private void update() {
		for (Entry<String, String> entry : m_coreAliases.entrySet()) {
			if (m_commands.containsKey(entry.getValue())){
				m_commands.put(entry.getKey(), m_commands.get(entry.getValue()));
			}else{
				m_commands.remove(entry.getValue());
			}
		}
		
		Properties props = new Properties();
		props.put(CommandProcessor.COMMAND_SCOPE, "bootstrap");
		props.put(CommandProcessor.COMMAND_FUNCTION, m_commands.keySet().toArray(new String[m_commands.keySet().size()]));
		
		if (m_bootstrapHandlerComponent != null){
			m_dm.remove(m_bootstrapHandlerComponent);
		}
		
		m_bootstrapHandlerComponent = m_dm.createComponent()
				.setInterface(BootstrapHandler.class.getName(), props)
				.setImplementation(new BootstrapHandler())
				.add(m_dm.createServiceDependency().setService(Navigator.class).setRequired(true));
		m_dm.add(m_bootstrapHandlerComponent);
	}

	private String getPluginName(BootstrapPlugin c) {
		PluginInfo plugin = c.getClass().getAnnotation(PluginInfo.class);
		
		return plugin != null ? plugin.name() : "unnamed";
	}
	
	class BootstrapHandler {

		private volatile Navigator m_navigator;

		/*
		 * This is invokved for ANY bootstrap command. The name of the command
		 * is the first argument.
		 */

		public <T> Object _main(CommandSession session, String args[])
				throws Exception {
			ReporterAdapter reporter = new ReporterAdapter();

			//TODO: Only create prompt when interactive mode enabled
			Prompt prompt = new GogoPrompt(session);
			
			CommandLine cmdline = new CommandLine(reporter, prompt);

			ExtList<String> arguments = new ExtList<>(args);


			/*
			 * Replace command alias with the original command 
			 */
			String cmd = arguments.remove(0);
			if (m_coreAliases.containsKey(cmd)){
				cmd = m_coreAliases.get(cmd);
			}
			
			BootstrapPlugin target = m_commands.get(cmd);
			

			/*
			 * Validate command scope
			 */
			Map<String, Method> commands = cmdline.getCommands(target);
			Method m = commands.get(cmd);
			
			Command annotation=null;
			/*
			 * For a virtual command like help there is no method
			 */
			if (m != null){
				annotation = m.getAnnotation(Command.class);
			}
			
			if (annotation != null){
				Scope scope = annotation.scope();
				
				if (Scope.WORKSPACE.equals(scope) && m_navigator.getWorkspaceDir() == null){
					session.getConsole().println("You need to be in a workspace to execute this command");
					return null;
							
				}else if(Scope.PROJECT.equals(scope) && m_navigator.getProjectDir() == null){
					session.getConsole().println("You need to be in a project to execute this command");
					return null;
				}
			}
			
			/*
			 * Execute command
			 */
			String help = cmdline.execute(target, cmd, arguments);

			/*
			 * Error
			 */
			if (!reporter.isOk()) {
				reporter.report(session.getConsole());
				if (help != null){
					session.getConsole().println(help);
				}
				return null;
			}
			
			/*
			 * Help command
			 */
			if (help != null) {
				session.getConsole().println(help);
				return null;
			}

			return cmdline.getResult();
		}
		
	}
	
	/*
	 * Return a stream of commands based on an object. Each public instance
	 * method, that starts with a '_' or annotated with @Command is considered a command.
	 */
	private Stream<String> getCommands(BootstrapPlugin plugin) {
		String pluginName = getPluginName(plugin);
		
		return stream(plugin.getClass().getMethods()) //
				.filter(m -> !Modifier.isStatic(m.getModifiers())
						&& (m.isAnnotationPresent(Command.class)
						|| m.getName().startsWith("_")))//
				.map(m -> String.format("%s-%s", pluginName.toLowerCase(),  m.getName().toLowerCase()));
	}
	
	@Override
	public List<PluginDescription> listPlugins() {
		return m_commands.keySet().stream()
				.filter(name -> name.contains("-"))
				.map(name -> name.substring(0, name.indexOf("-")))
				.distinct()
				.map(p -> {
					PluginDescriptionBuilder pluginDescriptionBuilder = new PluginDescriptionBuilder(p);
					String commandName = m_commands.keySet().stream().filter(k -> k.startsWith(p)).findFirst().get();
					BootstrapPlugin bootstrapPlugin = m_commands.get(commandName);
					Map<String, Method> commands = getCommands(bootstrapPlugin.getClass());
					
					commands.values().forEach(c -> {
						pluginDescriptionBuilder.command(c);
					});
					
					return pluginDescriptionBuilder.build();
				}).collect(Collectors.toList());
	}

	
	public Map<String,Method> getCommands(Class<?> target) {
		Map<String,Method> map = new TreeMap<String,Method>();

		for (Method m : target.getMethods()) {

			if (m.getName().startsWith("_") || m.isAnnotationPresent(Command.class)) {
					String name = m.getName(); 
					
				PluginInfo pluginInfo = target.getAnnotation(PluginInfo.class);
				map.put(pluginInfo.name().toLowerCase() + "-" + name.toLowerCase(), m);
				
			}
		}
		return map;
	}

}
