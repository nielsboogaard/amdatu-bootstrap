package org.amdatu.bootstrap.webui;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.Start;

@Component
public class BrowserOpener {

	@Start
	public void start() throws IOException, URISyntaxException {
		Desktop.getDesktop().browse(new URI("http://localhost:8080/ui/index.html"));
	}
}
