// <reference path="typeScriptDefenitions/libs.d.ts" />

import PluginsService = require('PluginsService')

class PluginsController {
    dirs : string[] = [];
    currentDir : string;

    plugins = [];
    command : string;
    arguments = [];
    argValues = {};
    output : string[] = [];

    static $inject = ['PluginsService', '$scope', '$modal'];

    constructor(private pluginsService : PluginsService, private scope : ng.IScope, private modal : ng.ui.bootstrap.IModalService) {
        pluginsService.getPlugins().flatMap(plugin => {
            return Rx.Observable.fromArray(plugin.commandDescription).flatMap(c => {
                c.name = plugin.name + "-" + c.name;
                return Rx.Observable.of(c);
            });
        }).subscribe(p => this.plugins.push(p));

        pluginsService.connect().do(r => {

            if (r.id) {
                console.log(r);
                var modalInstance = this.modal.open({
                    templateUrl: 'myModalContent.html',
                    controller: 'FeedbackModalController',
                    resolve: {
                        question: () => r
                    }
                });

                modalInstance.result.then(function (selectedItem) {
                    console.log("closed with", selectedItem);
                    pluginsService.sendAnswer({
                        //id: r.id,
                        type: 'promptquestion',
                        answer: selectedItem
                    });
                }, function () {
                    console.log('Modal dismissed, this is not handled yet');
                });

            } else if(r.successful == false) {
                this.writeErrorToOutput(r.result);
                scope.$digest();
            }
            else if(r.commandName == 'navigation-cd') {
                console.log("changed dir", r.result);
                this.currentDir = r.result;
                this.dirs = [];
                this.listDirs();
            }
            else {
                this.writeToOutput(r.result);
                console.log("Command result: ", r);
                scope.$digest();
            }


        }).subscribe();

        this.listDirs();

    }

    private listDirs() {
        this.pluginsService.ls()
            .map(d => d.substr(d.lastIndexOf("/") + 1))
            .subscribe(d => {
                if(this.dirs.indexOf(d) == -1) {
                    this.dirs.push(d)
                }
            });
    }

    execute() {
        this.pluginsService.execute(this.command, this.argValues);

        this.command = null;
        this.arguments = [];
        this.argValues = {};
    }

    navigate(dir) {
        this.dirs = [];
        this.pluginsService.navigate(dir);
    }

    private writeToOutput(result) {
       if(result && result.installedDependencies) {
           var installed = "Installed: ";
           result.installedDependencies.forEach(i => installed += i);

           var skipped = "Skipped: ";
           result.skippedDependencies.forEach(s => {
               if(s.version) {
                   skipped += s.bsn + ";" + s.version + ", "
               } else {
                   skipped += s.bsn + ", ";
               }

           });

           this.output.push(installed);
           this.output.push(skipped);
           this.output.push("Updated: " + result.updatedDependencies);


       } else {
           this.output.push(result);
       }

        console.log(this.output);

    }

    private writeErrorToOutput(result) {
        this.output.push("Error: " + result.message);
    }

    showArgs() {
        this.arguments = [];

        Rx.Observable.fromArray(this.plugins)
            .filter(p => p.name == this.command)
            .flatMap(p => {
                return Rx.Observable.fromArray(p.arguments)
            })
            .filter(a => a['name'].lastIndexOf("_", 0) != 0 && a['name'] != "arg1")
            .subscribe(a => {
                this.arguments.push(a);
            });
    }
}

export = PluginsController