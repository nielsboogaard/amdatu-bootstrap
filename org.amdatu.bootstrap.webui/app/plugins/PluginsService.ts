/// <reference path="../typescriptDefinitions/libs.d.ts" />

import Rx = require('Rx')
import SockJS = require('SockJS')
import Stomp = require('Stomp')
import Atmosphere = require('Atmosphere')

class PluginsService {
    static $inject = ['$http', '$q', 'BASE_URL', '$rootScope'];

    private request;

    constructor(private $http:ng.IHttpService, private $q:ng.IQService, private BASE_URL, private $rootScope : ng.IRootScopeService) {
    }

    getPlugins() : Rx.Observable<Plugin> {
        var observable = Rx.Observable.create((observer : Rx.Observer<Plugin>) => {
            this.$http.get(this.BASE_URL + "/bootstrap").success((resp) => {
                Rx.Observable.fromArray(resp).subscribe(
                    p => observer.onNext(<Plugin>p),
                    e => console.log(e),
                    () => observer.onCompleted());
            });
        });

        return observable;
    }

    ls() : Rx.Observable<string> {
        var observable = Rx.Observable.create((observer : Rx.Observer<string>) => {
            this.$http.get(this.BASE_URL + "/bootstrap/listdirs").success((resp) => {
                Rx.Observable.fromArray(resp).subscribe(
                    p => observer.onNext(<string>p),
                    e => console.log(e),
                    () => observer.onCompleted());
            });
        });

        return observable;
    }

    execute(command : string, argValues : {}) {
        if(argValues['arguments'] != undefined) {
            argValues['arguments'] = argValues['arguments'].split(" ");
        }

        console.log("executing command");
        this.request.push(JSON.stringify({name: command, args: argValues, type: 'command'}));

    }

    navigate(dir : string) {
        this.execute('navigation-cd', {arguments: dir});
    }

    sendAnswer(answer) {
        var data = JSON.stringify(answer);
        console.log(data);
        this.request.push(data);
        console.log("sending....");
    }

    connect() : Rx.Observable<any>{
        var observable = Rx.Observable.create((observer : Rx.Observer<any>) => {

            console.log("connecting");
            this.request = Atmosphere.subscribe({
                url: 'http://localhost:8080/atmosphere/bootstrap',
                contentType : "application/json",
                transport: 'websocket',
                trackMessageLength: false,
                onOpen: (resp) => {
                    console.log("Open!");
                    //   this.request.push(JSON.stringify({msg: 'test'}));
                },
                onError: (e) => console.log(e),
                onMessage: (m) => {
                    var value = JSON.parse(m.responseBody);


                    observer.onNext(value);
                },
                onClose: () => console.log("Close"),
                onClientTimeout: () => console.log('timeout'),
                onTransportFailure: () => console.log("failure"),
                logLevel: 'debug'
            });
        });

        return observable;
    }

}

export = PluginsService;