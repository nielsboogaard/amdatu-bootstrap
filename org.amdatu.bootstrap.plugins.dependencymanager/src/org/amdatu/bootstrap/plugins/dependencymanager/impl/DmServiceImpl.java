package org.amdatu.bootstrap.plugins.dependencymanager.impl;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

import org.amdatu.bootstrap.core.Dependency;
import org.amdatu.bootstrap.core.DependencyBuilder;
import org.amdatu.bootstrap.core.Feedback;
import org.amdatu.bootstrap.core.FullyQualifiedName;
import org.amdatu.bootstrap.core.InstallResult;
import org.amdatu.bootstrap.core.Navigator;
import org.amdatu.bootstrap.core.ResourceManager;
import org.amdatu.bootstrap.plugins.dependencymanager.DmService;
import org.amdatu.template.processor.TemplateContext;
import org.amdatu.template.processor.TemplateEngine;
import org.amdatu.template.processor.TemplateException;
import org.amdatu.template.processor.TemplateProcessor;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.Inject;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.osgi.framework.BundleContext;

import aQute.bnd.build.model.BndEditModel;
import aQute.bnd.build.model.clauses.HeaderClause;
import aQute.bnd.header.Attrs;
import aQute.bnd.properties.Document;

@Component
public class DmServiceImpl implements DmService {
	private static final String ANNOTATION_PLUGIN_NAME = "org.apache.felix.dm.annotation.plugin.bnd.AnnotationPlugin";

	private static final String ANNOTATION_PROCESSOR_JAR = "org.apache.felix.dependencymanager.annotation-3.1.1-SNAPSHOT.jar";

	@ServiceDependency
	private volatile DependencyBuilder m_dependencyBuilder;

	@ServiceDependency
	private volatile Navigator m_navigator;

	@ServiceDependency
	private volatile ResourceManager m_resourceManager;

	@ServiceDependency(required = true)
	private volatile TemplateEngine m_templateEngine;

	@ServiceDependency 
	private volatile Feedback m_feedback;
	
	@Inject
	private volatile BundleContext m_bundleContext;
	
	@Override
	public InstallResult installAnnotationProcessor() {
		Path mainBnd = m_navigator.getWorkspaceDir().resolve("cnf/build.bnd");

		try {
			String mainBndContents = new String(Files.readAllBytes(mainBnd));

			if (!mainBndContents.contains(ANNOTATION_PLUGIN_NAME)) {
				BndEditModel model = new BndEditModel();
				model.loadFrom(mainBnd.toFile());

				List<HeaderClause> plugins = model.getPlugins();
				if (plugins == null) {
					plugins = new ArrayList<>();
				}

				if (plugins.isEmpty()) {
					plugins.add(createRepoPluginHeader());
				}

				plugins.add(createAnnotationPluginHeader());

				model.setPlugins(plugins);

				Document document = new Document(mainBndContents);
				model.saveChangesTo(document);

				m_resourceManager.writeFile(mainBnd, document.get().getBytes());

				Path pluginsDir = m_navigator.getWorkspaceDir().resolve("cnf/plugins");
				try (InputStream in = m_bundleContext.getBundle().getEntry("/libs/" + ANNOTATION_PROCESSOR_JAR).openStream()) {
					Files.copy(in, pluginsDir.resolve(ANNOTATION_PROCESSOR_JAR), StandardCopyOption.REPLACE_EXISTING);
				}
			}
			
			return m_dependencyBuilder.addDependency("../cnf/plugins/org.apache.felix.dependencymanager.annotation-3.1.1-SNAPSHOT.jar", "file");
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	private HeaderClause createRepoPluginHeader() {
		Attrs pluginAttrs = new Attrs();
		HeaderClause header = new HeaderClause("${ext.repositories.-plugin}", pluginAttrs);
		return header;
	}

	private HeaderClause createAnnotationPluginHeader() {
		Attrs pluginAttrs = new Attrs();
		pluginAttrs.put("path:", "${plugin-dir}/" + ANNOTATION_PROCESSOR_JAR);
		pluginAttrs.put("build-import-export-service", "false");
		HeaderClause header = new HeaderClause(ANNOTATION_PLUGIN_NAME, pluginAttrs);
		return header;
	}

	@Override
	public InstallResult addDependencies() {
		return m_dependencyBuilder.addDependencies(Dependency.fromStrings("org.apache.felix.dependencymanager", "osgi.core"));
	}
	
	public void addComponent(boolean useAnnotations, FullyQualifiedName interfaceName, FullyQualifiedName componentName) {
		URL templateUri;
		if (useAnnotations) {
			templateUri = m_bundleContext.getBundle().getEntry("/templates/annotatedcomponent.vm");
		} else {
			templateUri = m_bundleContext.getBundle().getEntry("/templates/component.vm");
		}

		String activatorInterfaceName = null;
		
		createComponentFile(componentName.getClassName() + ".java", componentName, interfaceName, templateUri);

		if (!useAnnotations) {
			createActivator(componentName, new FullyQualifiedName(activatorInterfaceName));
		}
	}

	private void createComponentFile(String fileName, FullyQualifiedName fqn, FullyQualifiedName interfaceName, URL templateUri) {
		try {
			TemplateProcessor processor = m_templateEngine.createProcessor(templateUri);
			TemplateContext context = m_templateEngine.createContext();
			context.put("componentName", fqn.getClassName());
			context.put("interfaceName", interfaceName);
			context.put("packageName", fqn.getPackageName());

			String template = processor.generateString(context);
			Path outputDir = m_navigator.getCurrentDir().resolve("src").resolve(fqn.getPackageName().replaceAll("\\.", "/"));
			Path outputFile = outputDir.resolve(fileName);
			Files.createDirectories(outputDir);

			m_feedback.println("Created file: " + outputFile);
			Files.write(outputFile, template.getBytes());

		} catch (TemplateException | IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public void createActivator(FullyQualifiedName fqn, FullyQualifiedName interfaceName) {
		URL activatorTemplate = m_bundleContext.getBundle().getEntry("/templates/activator.vm");
		createComponentFile("Activator.java", fqn, interfaceName, activatorTemplate);
		String activator = "\nBundle-Activator: " + fqn.getPackageName() + ".Activator";
		
		try {
			Files.write(m_navigator.getBndFile(), activator.getBytes(), StandardOpenOption.APPEND);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public InstallResult addRunDependencies(Path runFilePath) {
		return m_dependencyBuilder.addRunDependency(
				Dependency.fromStrings(
						"org.apache.felix.dependencymanager", 
						"org.apache.felix.dependencymanager.runtime",
						"org.apache.felix.dependencymanager.shell",
						"org.apache.felix.metatype",
						"org.apache.felix.eventadmin",
						"org.apache.felix.configadmin",
						"org.apache.felix.log"), runFilePath);
	}
}
