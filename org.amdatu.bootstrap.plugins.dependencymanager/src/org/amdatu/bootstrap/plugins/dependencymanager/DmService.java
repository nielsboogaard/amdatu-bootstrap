package org.amdatu.bootstrap.plugins.dependencymanager;

import java.nio.file.Path;

import org.amdatu.bootstrap.core.FullyQualifiedName;
import org.amdatu.bootstrap.core.InstallResult;

public interface DmService {
	InstallResult installAnnotationProcessor();
	InstallResult addDependencies();
	void createActivator(FullyQualifiedName fqn, FullyQualifiedName interfaceName);
	InstallResult addRunDependencies(Path runFilePath);
}
