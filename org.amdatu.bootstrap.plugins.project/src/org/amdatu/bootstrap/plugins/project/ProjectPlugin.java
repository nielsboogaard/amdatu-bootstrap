package org.amdatu.bootstrap.plugins.project;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.amdatu.bootstrap.core.BootstrapPlugin;
import org.amdatu.bootstrap.core.Command;
import org.amdatu.bootstrap.core.Feedback;
import org.amdatu.bootstrap.core.Navigator;
import org.amdatu.bootstrap.core.PluginInfo;
import org.amdatu.bootstrap.core.Prompt;
import org.amdatu.bootstrap.getopt.Arguments;
import org.amdatu.bootstrap.getopt.Description;
import org.amdatu.bootstrap.getopt.Parameters;
import org.amdatu.bootstrap.template.Template;
import org.amdatu.bootstrap.template.TemplateException;
import org.amdatu.bootstrap.template.TemplateProcessor;
import org.amdatu.bootstrap.template.TemplateProvider;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventAdmin;

@Component
@PluginInfo(name = "project")
public class ProjectPlugin implements BootstrapPlugin {
	
	@ServiceDependency
	private EventAdmin m_eventAdmin;
	
	@ServiceDependency
	private volatile Navigator m_navigator;
	
	@ServiceDependency
	private volatile Feedback m_feedback;
	
	@ServiceDependency
	private volatile TemplateProcessor m_templateProcessor;
	
	private final Set<TemplateProvider> m_projectTemplateProviders = new HashSet<>();
	
	@Arguments(arg="name")
	interface CreateParameters extends Parameters {
		@Description("Name of the project")
		String[] arguments();
		
		@Description("Name of the template")
		String template();
	}
	
	@Command
	public File create(CreateParameters parameters, Prompt prompt) {
		if (m_navigator.getProjectDir() != null){
			m_feedback.println("Can't create project (already in a project)");
			return null;
		}
		
		String name = parameters.arguments()[0];
		
		File file = new File(m_navigator.getCurrentDir().toFile(), name);
		if (file.exists()){
			m_feedback.println("Can't create project (file exists)");
			return null;
		}
		
		if (!file.mkdir()){
			m_feedback.println("Can't create project (couldn't create project dir)");
			return null;
		}
		
		try {
			Template template = determineTemplate(parameters, prompt, m_projectTemplateProviders);
			
			Map<String, Object> context = new HashMap<>();
			context.put("projectName", name);

			m_templateProcessor.installTemplate(template, file, context);
			
			m_navigator.changeDir(file.toPath());
			
			m_feedback.println(String.format("Created new project '%s' using template '%s'", name, template.getName()));

			
			sendEvent("org/amdatu/bootstrap/core/PROJECT_CREATED", name);

		} catch (TemplateException e) {
			throw new RuntimeException(e);
		}
		
		return null;
	}

	private Template determineTemplate(CreateParameters parameters, Prompt prompt, Set<TemplateProvider> templateProviders) {
		List<Template> templateOptions = new ArrayList<>();
		
		
		for (TemplateProvider p : templateProviders){
			templateOptions.addAll(p.listTemplates());
		}

		Template template = null;
		if (parameters.template() != null){
			for (Template t : templateOptions) {
				if (t.getName().equals(parameters.template())){
					template = t;
					break;
				}
			}
			if (template == null){
				m_feedback.println(String.format("Template '%s' not found", parameters.template()));
			}
			
		} else {
			if (templateOptions.size() == 0){
				m_feedback.println("No project templates found");
			} else if (templateOptions.size() == 1){
				template = templateOptions.get(0);
			} else {
				template = prompt.askChoice("Which template do you want to use", 0, templateOptions);
			}
			
		}
		return template;
	}
		
	@ServiceDependency(removed="templateProviderRemoved", filter="(type=project)")
	private void templateProviderAdded(TemplateProvider templateProvider){
		m_projectTemplateProviders.add(templateProvider);
	}
	
	@SuppressWarnings("unused" /* dependency manager callback */)
	private void templateProviderRemoved(TemplateProvider templateProvider) {
		m_projectTemplateProviders.remove(templateProvider);
	}
	
	private void sendEvent(String topicName, String name) {
	    Map<String, Object> props = new HashMap<>();
	    props.put("projectname", name);
	    Event event = new Event(topicName, props);
	    m_eventAdmin.sendEvent(event);
	}
}
