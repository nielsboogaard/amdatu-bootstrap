package org.amdatu.bootstrap.ws;

public class CommandResult {
	private final String m_commandName;
	private final Object m_result;
	private final boolean m_successful;

	public CommandResult(String commandName, Object result, boolean successful) {
		m_commandName = commandName;
		m_result = result;
		m_successful = successful;
	}

	public CommandResult(String commandName, Object result) {
		m_commandName = commandName;
		m_result = result;
		m_successful = true;
	}

	public String getCommandName() {
		return m_commandName;
	}

	public Object getResult() {
		return m_result;
	}

	public boolean isSuccessful() {
		return m_successful;
	}
}
