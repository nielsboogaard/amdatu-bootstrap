package org.amdatu.bootstrap.ws;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.amdatu.bootstrap.core.FullyQualifiedName;
import org.amdatu.bootstrap.core.Prompt;
import org.atmosphere.cpr.AtmosphereResource;

import rx.observables.BlockingObservable;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class AtmospherePrompt implements Prompt {

	private AtmosphereResource m_resource;
	private BlockingObservable<Object> m_observable;
	
	public AtmospherePrompt(AtmosphereResource resource, BlockingObservable<Object> observable) {
		this.m_resource = resource;
		this.m_observable = observable;
	}
	
	@Override
	public boolean askBoolean(String message, boolean defaultChoice) {
		String stringValue = createPromptQuestion(message, String.valueOf(defaultChoice), Boolean.class);
		return Boolean.parseBoolean(stringValue);
	}

	@Override
	public String askString(String message) {
		return createPromptQuestion(message, null, String.class);
	}

	@Override
	public String askString(String message, String defaultString) {
		return createPromptQuestion(message, defaultString, getClass());
	}

	@Override
	public <T> T askChoice(String message, int defaultOption, List<T> options) {
		int selectedIndex = createChoicePromptQuestion(message, defaultOption, options);
		return options.get(selectedIndex);
	}

	@Override
	public int askChoiceAsIndex(String message, int defaultOption,
			List<? extends Object> options) {
		return createChoicePromptQuestion(message, defaultOption, options);
	}

	private int createChoicePromptQuestion(String message, int defaultOption, List<? extends Object> options) {
		PromptQuestion q = new PromptQuestion();
		q.setId(UUID.randomUUID().toString());
		q.setType(Integer.class.getName());
		q.setDescription(message);
		q.setDefaultOption(defaultOption);
		q.setOptions(options.stream().map(o -> o.toString()).collect(Collectors.toList()));

		ObjectMapper mapper = new ObjectMapper();

		try {
			m_resource.write(mapper.writeValueAsString(q));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		
		Iterable<Object> next = m_observable.next();
		
		String choiceStr = (String)next.iterator().next();
		return Integer.parseInt(choiceStr);
	}

	@Override
	public FullyQualifiedName askComponentName() {
		String answer = createPromptQuestion("What is the name of the component", null, FullyQualifiedName.class);
		
		return new FullyQualifiedName(answer);
	}

	@Override
	public FullyQualifiedName askComponentName(String message) {
		String answer = createPromptQuestion(message, null, FullyQualifiedName.class);
		return new FullyQualifiedName(answer);
	}

	@Override
	public FullyQualifiedName askComponentName(String message, String defaultName) {
		String answer = createPromptQuestion("What is the name of the component", defaultName, FullyQualifiedName.class);
		return new FullyQualifiedName(answer);
	}
	
	private String createPromptQuestion(String message, String defaultValue, Class<?> type) {
		PromptQuestion q = new PromptQuestion();
		q.setId(UUID.randomUUID().toString());
		q.setType(type.getName());
		q.setDescription(message);
		q.setDefaultValue(defaultValue);

		ObjectMapper mapper = new ObjectMapper();

		try {
			m_resource.write(mapper.writeValueAsString(q));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		
		Iterable<Object> next = m_observable.next();
		return (String) next.iterator().next();
	}

}
