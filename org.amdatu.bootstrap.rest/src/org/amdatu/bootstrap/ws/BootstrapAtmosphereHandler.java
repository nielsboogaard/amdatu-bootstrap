package org.amdatu.bootstrap.ws;

import static java.util.Arrays.stream;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.amdatu.bootstrap.core.BootstrapPlugin;
import org.amdatu.bootstrap.core.Command;
import org.amdatu.bootstrap.core.CommandNotFoundException;
import org.amdatu.bootstrap.core.Navigator;
import org.amdatu.bootstrap.core.PluginInfo;
import org.amdatu.bootstrap.core.PluginRegistry;
import org.amdatu.bootstrap.core.Prompt;
import org.amdatu.bootstrap.rest.CommandInput;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.Property;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.atmosphere.cpr.AtmosphereResource;
import org.atmosphere.cpr.AtmosphereResponse;
import org.atmosphere.handler.OnMessage;
import org.atmosphere.websocket.WebSocketEventListenerAdapter;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventConstants;
import org.osgi.service.event.EventHandler;

import rx.Observable;
import rx.Subscriber;
import aQute.bnd.annotation.metatype.Configurable;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component(
		provides = { OnMessage.class, EventHandler.class },
		properties = {@Property(name=EventConstants.EVENT_TOPIC, values=Navigator.CHANGEDIR_TOPIC)}
		)
public class BootstrapAtmosphereHandler extends OnMessage<String> implements EventHandler {
	
	private final Map<String, PluginCommandTuple> m_commands = new ConcurrentHashMap<>();

	@ServiceDependency
	private volatile PluginRegistry m_pluginRegistry;

	@ServiceDependency
	private volatile Navigator m_navigator;
	
	private volatile AtmosphereResource m_resource;
	
	private volatile ExecutorService m_commandExecutor = Executors.newSingleThreadExecutor();
	
	private volatile Subscriber<Object> m_subscriber;
	
	@ServiceDependency(removed = "removeCommand")
	public void addCommand(BootstrapPlugin plugin) throws Exception {
		getCommands(plugin).forEach(c -> {
			PluginCommandTuple tuple = new PluginCommandTuple();
			tuple.plugin = plugin;
			tuple.commandMethod = c;

			m_commands.put(getPluginName(plugin) + "-" + c.getName(), tuple);
		});
	}

	private String getPluginName(BootstrapPlugin c) {
		PluginInfo plugin = c.getClass().getAnnotation(PluginInfo.class);

		return plugin != null ? plugin.name() : "unnamed";
	}

	public void removeCommand(BootstrapPlugin plugin) {
		getCommands(plugin).forEach(c -> m_commands.remove(getPluginName(plugin) + "-" + c.getName()));
	}

	private Stream<Method> getCommands(BootstrapPlugin plugin) {
		return stream(plugin.getClass().getMethods()) //
				.filter(m -> !Modifier.isStatic(m.getModifiers())
						&& (m.isAnnotationPresent(Command.class)
						|| m.getName().startsWith("_")));
	}
	


	
	@Override
	public void onOpen(AtmosphereResource resource) throws IOException {
		super.onOpen(resource);
		
		m_resource = resource;
		
		Observable<Object> observable = Observable.create(s-> m_subscriber = s);		
		Prompt atmospherePrompt = new AtmospherePrompt(resource, observable.toBlocking());
		
		resource.addEventListener(new WebSocketEventListenerAdapter() {
			@Override
			public void onMessage(@SuppressWarnings("rawtypes") WebSocketEvent  event) {
				ObjectMapper mapper = new ObjectMapper();
				
				WsMessage wsMessage;
				try {
					wsMessage = mapper.readValue(event.message().toString(), WsMessage.class);

					switch (wsMessage.getType()) {
					case "promptquestion":
						handlePromptQuestion(event, mapper);
						break;
					case "command":
						m_commandExecutor.execute(() -> {
							try {
								handleCommand(event, mapper, atmospherePrompt);
							} catch (Exception ex) {
								sendError(event, mapper, ex);
							}
						});
						break;
					default: System.out.println("Unrecognized message: " + wsMessage);
					}
				} catch (IOException e) {
					e.printStackTrace();
				}

			}

			private void handleCommand(@SuppressWarnings("rawtypes") WebSocketEvent event, ObjectMapper mapper, Prompt atmospherePrompt) throws JsonParseException, JsonMappingException, IOException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
				CommandInput input = mapper.readValue(event.message().toString(), CommandInput.class);
				
				PluginCommandTuple pluginCommandTuple = m_commands.get(input.getName());

				if(pluginCommandTuple == null) {
					throw new CommandNotFoundException(input.getName());
				}
				
				Class<?>[] parameterTypes = pluginCommandTuple.commandMethod.getParameterTypes();
				Object result;
				
				if (parameterTypes.length > 0) {
					List<?> configurables = Arrays.stream(parameterTypes)
							.map(p -> {
								if(p.isAssignableFrom(Prompt.class)) {
									return atmospherePrompt;
								} else {
									return Configurable.createConfigurable(p, input.getArgs());
								}
								
							}).collect(Collectors.toList());
					
					
					Object[] params = new Object[configurables.size()];
					
					result = pluginCommandTuple.commandMethod.invoke(pluginCommandTuple.plugin, configurables.toArray(params));
					
				} else {
					result = pluginCommandTuple.commandMethod.invoke(pluginCommandTuple.plugin);
				}
				
				resource.write(mapper.writeValueAsBytes(new CommandResult(input.getName(), result)));
				
			}

			private void handlePromptQuestion(@SuppressWarnings("rawtypes") WebSocketEvent event, ObjectMapper mapper) throws IOException,
					JsonParseException, JsonMappingException {
				PromptQuestion q = mapper.readValue(event.message().toString(), PromptQuestion.class);
				m_subscriber.onNext(q.getAnswer());
			}
			
			private void sendError( @SuppressWarnings("rawtypes") WebSocketEvent event, ObjectMapper mapper, Exception ex) {
				try {
					CommandInput input = mapper.readValue(event.message().toString(), CommandInput.class);
					resource.write(mapper.writeValueAsBytes(new CommandResult(input.getName(), ex, false)));
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
	}

	@Override
	public void onMessage(AtmosphereResponse arg0, String arg1)
			throws IOException {
	}
	
	@Override
	public void onDisconnect(AtmosphereResponse response) throws IOException {
		super.onDisconnect(response);
		m_resource = null;
	}



	class PluginCommandTuple {
		BootstrapPlugin plugin;
		Method commandMethod;
	}

	@Override
	public void handleEvent(Event event) {
		if(event.getTopic().equals(Navigator.CHANGEDIR_TOPIC)) {
			Path dir = (Path)event.getProperty("dir");
			ObjectMapper mapper = new ObjectMapper();
			
			try {
				m_resource.write(mapper.writeValueAsBytes(new CommandResult("navigation-cd", dir.toFile())));
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}
		}
	}
}
