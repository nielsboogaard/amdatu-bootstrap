package org.amdatu.bootstrap.ws;

import org.amdatu.bootstrap.core.BootstrapPlugin;
import org.amdatu.bootstrap.core.Command;
import org.amdatu.bootstrap.core.PluginInfo;
import org.amdatu.bootstrap.core.Prompt;
import org.apache.felix.dm.annotation.api.Component;

@Component
@PluginInfo(name="ws")
public class ExamplePromptCommand implements BootstrapPlugin{
	
	@Command
	public void demo(Prompt prompt) {
		System.out.println("Demo");
		
		String askString = prompt.askString("test question");
		System.out.println("Answer: " + 	askString);
		
		boolean askBool = prompt.askBoolean("test boolean question", false);
		System.out.println("Boolean: " + askBool);
		
		System.out.println("Demo end");
	}
	
}
