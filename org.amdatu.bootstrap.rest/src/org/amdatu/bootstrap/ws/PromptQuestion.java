package org.amdatu.bootstrap.ws;

import java.util.List;

public class PromptQuestion {
	private String m_id;
	private String m_type;
	private String m_description;
	private String m_defaultValue;
	private List<String> m_options;
	private Integer m_defaultOption;
	private String m_answer;

	public String getId() {
		return m_id;
	}

	public void setId(String id) {
		m_id = id;
	}

	public String getType() {
		return m_type;
	}

	public void setType(String type) {
		m_type = type;
	}

	public String getDescription() {
		return m_description;
	}

	public void setDescription(String description) {
		m_description = description;
	}
	
	public String getDefaultValue() {
		return m_defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		m_defaultValue = defaultValue;
	}
	
	public List<String> getOptions() {
		return m_options;
	}

	public void setOptions(List<String> options) {
		m_options = options;
	}

	public Integer getDefaultOption() {
		return m_defaultOption;
	}

	public void setDefaultOption(Integer defaultOption) {
		m_defaultOption = defaultOption;
	}

	public String getAnswer() {
		return m_answer;
	}

	public void setAnswer(String answer) {
		m_answer = answer;
	}

}
