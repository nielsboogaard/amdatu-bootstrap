package org.amdatu.bootstrap.rest;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class CommandInput {
	private String m_name;
	private Map<String, Object> m_args;

	public String getName() {
		return m_name;
	}

	public void setName(String name) {
		m_name = name;
	}

	public Map<String, Object> getArgs() {
		return m_args;
	}

	public void setArgs(Map<String, Object> args) {
		m_args = args;
	}

}
