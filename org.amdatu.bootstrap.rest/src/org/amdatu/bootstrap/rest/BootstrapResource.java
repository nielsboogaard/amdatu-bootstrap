package org.amdatu.bootstrap.rest;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.amdatu.bootstrap.core.Navigator;
import org.amdatu.bootstrap.core.PluginDescription;
import org.amdatu.bootstrap.core.PluginRegistry;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;

@Path("bootstrap")
@Component(provides = Object.class)
public class BootstrapResource {

	@ServiceDependency
	private volatile PluginRegistry m_pluginRegistry;

	@ServiceDependency
	private volatile Navigator m_navigator;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<PluginDescription> listPlugins() {
		return m_pluginRegistry.listPlugins();
	}

	@GET
	@Path("listdirs")
	@Produces(MediaType.APPLICATION_JSON)
	public List<File> ls() {
		List<File> asList = Arrays.asList(m_navigator.getCurrentDir().toFile().listFiles(f -> f.isDirectory()));
		return asList;
	}
}
